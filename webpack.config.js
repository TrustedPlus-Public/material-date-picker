const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");
const autoprefixer = require("autoprefixer");

const ENV_PROD = process.argv.includes('--env=prod');

function configure(env) {
    return [
        {
            name: "typescript",
            entry: {
                bundle: "./src/components/index.ts",
                styles: "./src/components/index.scss"
            },
            output: {
                filename: "[name].js",
                path: path.resolve(__dirname, "./lib"),
                library: "[name]"
            },
            resolve: {
                extensions: [".js", ".jsx", ".ts", ".tsx"]
            },

            module: {
                rules: [
                    {
                        test: /\.tsx?$/,
                        use: "ts-loader"
                    },
                    {

                        test: /\.scss$/,
                        use: ExtractTextPlugin.extract({
                            fallback: 'style-loader',
                            use: ['css-loader', 'postcss-loader', 'sass-loader'],
                            publicPath: './lib'
                        })
                    }
                ]
            },
            plugins: [
                new ExtractTextPlugin({
                    filename: "style.css",
                    allChunks: true
                }),
                new webpack.LoaderOptionsPlugin({
                    debug: false,
                    options: {
                        minimize: env === ENV_PROD,
                        resolve: {},
                        postcss: [
                            autoprefixer({
                                browsers: [
                                    'last 5 version',
                                    'ie >= 9'
                                ]
                            })
                        ]
                    }
                }),
                ENV_PROD ?
                    new webpack.optimize.UglifyJsPlugin({
                        sourceMap: env === ENV_PROD
                    }) : function () {
                    },
                // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
                // new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/)
            ],
            externals: ENV_PROD ? {
                "react": "React",
                "react-dom": "ReactDOM",
                "react-router": "ReactRouter"
            } : {}
        }
    ];
}

module.exports = configure;