export function RandomId(size: number = 20) {
    return btoa(Math.random().toString().slice(2)).slice(0, size);
}

export class AssocArray {

    static size(assoc: Assoc<any>) {
        return Object.keys(assoc).length;
    }

    static forEach<T>(assoc: Assoc<T>, cb: (item: T, index: string) => void): void;
    static forEach(assoc: Assoc<any>, cb: (item: any, index: string) => void): void;
    static forEach(assoc: Assoc<any>, cb: (item: any, index: string) => void) {
        for (let key in assoc) {
            cb(assoc[key], key);
        }
    }

    static map<T, U>(assoc: Assoc<T>, cb: (item: T, index: string) => U): U[];
    static map<U>(assoc: Assoc<any>, cb: (item: any, index: string) => U): U[];
    static map<U>(assoc: Assoc<any>, cb: (item: any, index: string) => U): U[] {
        let res: U[] = [];
        for (let key in assoc) {
            res.push(cb(assoc[key], key));
        }
        return res;
    }

    static filter<T>(assoc: Assoc<T>, cb: (item: any, index: string) => boolean): Assoc<T>;
    static filter(assoc: Assoc<any>, cb: (item: any, index: string) => boolean): Assoc<any>;
    static filter(assoc: Assoc<any>, cb: (item: any, index: string) => boolean): Assoc<any> {
        let res: Assoc<any> = {};
        for (let key in assoc) {
            if (cb(assoc[key], key))
                res[key] = assoc[key];
        }
        return res;
    }

    static sort<T>(assoc: Assoc<T>, cb: (a: { key: string; value: T }, b: { key: string; value: T }) => number): Assoc<T> {
        let res: Assoc<T> = {};
        let array: { key: string; value: T }[] = [];
        for (let key in assoc) {
            array.push({ key: key, value: assoc[key] });
        }
        array = array.sort(cb);
        array.forEach(item => res[item.key] = item.value);
        return res;
    }

}

/**
 * Возвращает первого родителя элемента с заданным именем класса
 * - если родитель не найден, то возвращает null
 *
 * @export
 * @param {HTMLElement} element
 * @param {string} name
 * @returns {HTMLElement}
 */
export function getParentByClass(element: HTMLElement, name: string): HTMLElement | null {
    if (element && element.parentElement) {
        if (element.parentElement.classList.contains(name))
            return element.parentElement;
        else
            return getParentByClass(element.parentElement, name);
    }
    return null;
}

/**
 * Экранирование символов в регулярном выражении
 *
 * @param {string} text
 * @returns
 */
export function RegExpEscape(text: string) {
    return text ? text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") : "";
}

/**
 *
 *
 * @export
 * @param {string} link
 * @param {number} [width=495]
 * @param {number} [height=495]
 */
export function windowOpener(link: string, width: number = 495, height: number = 495): void {
    let dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : (screen as any).left;
    let dualScreenTop = window.screenTop !== undefined ? window.screenTop : (screen as any).top;

    let w = window.innerWidth ? window.innerWidth : document.documentElement!.clientWidth ? document.documentElement!.clientWidth : screen.width;
    let h = window.innerHeight ? window.innerHeight : document.documentElement!.clientHeight ? document.documentElement!.clientHeight : screen.height;
    let left = ((w / 2) - (width / 2)) + dualScreenLeft;
    let top = ((h / 2) - (height / 2)) + dualScreenTop - 200;
    let f = window.open(link, "Login", `scrollbars=yes, toolbar=no, location=no, menubar=no, width=${width}, height=${height}, top=${top}, left=${left}`);

    // Puts focus on the newWindow
    if (window.focus) {
        f && f.focus();
    }
}