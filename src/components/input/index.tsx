import * as React from "react";
import * as classnames from "classnames";

import { ComponentColorSheme, ComponentSizeType } from "../types";
import { RandomId, getParentByClass } from "../core";
import { Icon } from "../icon";

export interface IInputFieldProps {
    className?: string;
    hidden?: boolean;
    readOnly?: boolean;
    autoFocus?: boolean;
    placeholder?: string;
    error?: string;
    name?: string;
    onKeyUp?: React.KeyboardEventHandler<HTMLInputElement>;
    onChange?: React.FormEventHandler<HTMLInputElement>;
    onBlur?: React.FocusEventHandler<HTMLInputElement>;
    onInput?: React.FocusEventHandler<HTMLInputElement>;
    onClick?: React.MouseEventHandler<HTMLInputElement>;
    disabled?: boolean;
    value?: string;
    defaultValue?: string;
    label?: string;
    required?: boolean;
    description?: string;
    type?: string;
    tabIndex?: number;
    autoComplete?: boolean;

    fontSize?: "display-1" | "display-2" | "display-3" | "display-4" | "headline" | "title" | "subheading" | "body-1" | "body-2" | "caption" | "button" | "input";
    color?: string;
    colorSheme?: ComponentColorSheme;
    icon?: string;
    ico?: string;
}

export interface IInputFieldState {
    focused?: boolean;
}

export class InputField extends React.Component<IInputFieldProps, IInputFieldState> {

    static defaultProps: IInputFieldProps = {
        fontSize: "input",
        colorSheme: "light" as ComponentColorSheme
    };

    id = RandomId();

    constructor(props: IInputFieldProps) {
        super(props);
        this.state = {
            focused: false
        };

        this.onFocusHandle = this.onFocusHandle.bind(this);
        this.onBlurHandle = this.onBlurHandle.bind(this);
    }

    input: HTMLInputElement;

    onFocusHandle(e: React.FocusEvent<HTMLInputElement>) {
        this.setState({ focused: true });
        this.props.onInput && this.props.onInput(e);
    }

    onBlurHandle(e: React.FocusEvent<HTMLInputElement>) {
        this.setState({ focused: false });
        this.props.onBlur && this.props.onBlur(e);
    }

    render() {
        let id = this.id;

        let label: JSX.Element | null = null;
        if (this.props.label) {
            const isActive = (this.props.value || this.props.defaultValue || (this.input && this.input.value));
            const isFocus = (this.state.focused);
            const classLabel = classnames([
                (isActive ? "active" : ""),
                (isFocus ? "focus" : ""),
                (this.props.fontSize ? `font-${this.props.fontSize}` : null),
                (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null),
                (this.props.color ? this.props.color : null)
            ]);
            label = (
                <label className={classLabel} onClick={(e) => { e.stopPropagation(); this.input.focus(); }}>
                    {this.props.label}
                    {this.props.required ? <span className="font-red">*</span> : null}
                </label>
            );
        }

        let error: JSX.Element | null = null;
        let description: JSX.Element | null = null;
        if (this.props.error) {
            error = (
                <div className="input-error">
                    <span className="error-color">{this.props.error}</span>
                </div>
            );
        }
        else if (this.props.description) {
            description = (
                <div
                    className={"input-description " + (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null)}>
                    <span>{this.props.description}</span>
                </div>
            );
        }

        let icon: JSX.Element | null = null;
        const isFocus = (this.state.focused);
        const classIcon = classnames([
            (isFocus ? "focus" : ""),
            (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null),
            (this.props.description || this.props.error ? "wd" : null)]);
        if (this.props.icon) {
            icon = (
                <Icon name={this.props.icon} desc={classIcon} />
            );
        }

        const classInput = classnames([
            (this.props.label ? "wl" : null),
            (this.props.description ? "wd" : null),
            (this.props.error ? "we" : null),
            (this.props.error ? " invalid" : ""),
            (this.props.fontSize ? `font-${this.props.fontSize}` : null),
            (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null),
            (this.props.color ? this.props.color : null)
        ]);

        const classInputField = classnames([
            (this.props.disabled ? "disabled" : null),
            (this.props.icon ? "wi" : null),
            (this.props.className || "")
        ]);

        const inputStyle = this.props.ico ? {
            background: `url(${this.props.ico})`,
            padding: "6px 6px 6px 0",
            backgroundRepeat: "no-repeat",
            backgroundPositionX: "right",
            backgroundPositionY: "center",
            cursor: "pointer",
            opacity: 0.87,
            backgroundSize: "20px",
        } : {};

        return (
            <div className={`field-input col ${classInputField}`} hidden={this.props.hidden}>
                {icon}
                <div className="inputBox">
                    <input
                        style={inputStyle}
                        ref={(e: any) => { this.input = e; }}
                        id={id}
                        hidden={this.props.hidden}
                        readOnly={this.props.readOnly}
                        autoFocus={this.props.autoFocus}
                        placeholder={this.props.placeholder}
                        type={this.props.type || "text"}
                        className={classInput}
                        name={this.props.name}
                        onKeyUp={this.props.onKeyUp}
                        onFocus={this.onFocusHandle}
                        onBlur={this.onBlurHandle}
                        onInput={this.props.onInput}
                        onChange={this.props.onChange}
                        onClick={this.props.onClick}
                        disabled={this.props.disabled}
                        value={this.props.value}
                        defaultValue={this.props.defaultValue}
                        tabIndex={this.props.tabIndex}
                        autoComplete={!this.props.autoComplete ? "off" : undefined} />
                    {label}
                    {error}
                    {description}
                </div>
            </div>
        );
    }

}