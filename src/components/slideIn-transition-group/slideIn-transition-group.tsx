import * as React from "react";
import * as CSSTransitionGroup from "react-addons-css-transition-group";
import {SlideInChild} from "./slideIn-child";
import ReactChild = React.ReactChild;

export type SlideDirection = "right"| "left"|"up"|"down";

interface ISlideInTransitionGroupProps {
    direction: SlideDirection;
    className?: string;
}

export class SlideInTransitionGroup extends React.Component<ISlideInTransitionGroupProps, {}> {
    render() {
        const transitionOptions = {
            className: "animation-wrapper-container",
            transitionName: "fade",
            transitionEnterTimeout: 450,
            transitionLeaveTimeout: 450
        };

        const {direction} = this.props;
        return (
            <div className={`animation-wrapper direction-${direction}`}>
                <CSSTransitionGroup {...transitionOptions}>
                    {React.cloneElement(<SlideInChild
                        enterDelay={0}
                        maxScale={0}
                        minScale={0}
                        className={`animation-wrapper-content ${this.props.className || ""}`}
                    />, {key: Math.random().toString(16).substr(-12), children: this.props.children})}
                </CSSTransitionGroup>
            </div>
        );
    }
}