import * as React from "react";
import * as classnames from "classnames";

export interface IIconProps {
    name: string;
    color?: string;
    desc?: string;
}

export class Icon extends React.Component<IIconProps, {}> {

    constructor(props: IIconProps) {
        super(props);
        this.state = {};
    }

    static defaultProps: IIconProps = {
        name: "",
    };

    render() {
        const {name, color, desc} = this.props;
        const className = classnames("material-icons", color, desc);
        return (
            <i className={className}>{name}</i>
        );
    }

}