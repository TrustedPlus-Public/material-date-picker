import * as React from "react";
import * as ReactDOM from "react-dom";
import {moment, ClockMode, Affix, DEFAULT_LABEL_CANCEL, DEFAULT_LABEL_OK} from "./index";
import {ClockItems} from "./clock-item";
import {TimeDisplay} from "./time-display";
import {assign} from "../core/object";
import {getAffix, rad2deg} from "../core/date-time-utils";
import {FlatButton} from "../button/index";
import * as classnames from "classnames";

const MUST_AUTOCHANGE_MODE = false;

function isMousePressed(event: any) {
    if (typeof event.buttons === "undefined") {
        return event.nativeEvent.which;
    }

    return event.buttons;
}

interface IClockProps {
    viewMode: "portrait" | "landscape";
    initialTime: string;
    onChangeMinutes: (time: string) => void;
    autoSelect?: boolean;
    onChangeHours?: (time: string) => void;
    onAcceptClick?: () => void;
    onDismissClick?: () => void;
    cancelLabel?: string;
    okLabel?: string;
}

interface IClockState {
    mode: ClockMode;
    selectedTime: string;
}

export class Clock extends React.Component<IClockProps, IClockState> {
    private center: any;
    private basePoint: any;
    private arrowAngle: number;
    private assignedProps: IClockProps;

    static defaultProps = {
        viewMode: "portrait",
        initialTime: moment().format(),
        onChangeHours: (time: string) => void 0,
        onChangeMinutes: (time: string) => void 0,
        autoSelect: false,
        onAcceptClick: () => void 0,
        onDismissClick: () => void 0,
        cancelLabel: DEFAULT_LABEL_CANCEL,
        okLabel: DEFAULT_LABEL_OK
    };

    constructor(props: IClockProps) {
        super(props);

        this.assignedProps = assign(Clock.defaultProps, props);

        const initialTime = props.initialTime || moment().toISOString();

        this.state = {
            mode: "h",
            selectedTime: initialTime
        };

        this.handleChangeHours = this.handleChangeHours.bind(this);
        this.handleChangeMinutes = this.handleChangeMinutes.bind(this);
        this.handleSelectAffix = this.handleSelectAffix.bind(this);
        this.handleChangeAffix = this.handleChangeAffix.bind(this);
        this.handleChangeArrowAngle = this.handleChangeArrowAngle.bind(this);
        this.setMode = this.setMode.bind(this);
        this.getTime = this.getTime.bind(this);
        this.getTimeByEvent = this.getTimeByEvent.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleAcceptClick = this.handleAcceptClick.bind(this);
    }

    shouldComponentUpdate(nextProps: IClockProps, nextState: IClockState) {
        const {selectedTime} = this.state;
        const nextSelectedTime = nextState.selectedTime;
        const affixChanged = getAffix(selectedTime) !== getAffix(nextSelectedTime);
        const selectedTimeChanged = nextSelectedTime !== selectedTime;
        const modeChanged = this.state.mode !== nextState.mode;
        const initialTimeChanged = this.props.initialTime !== nextProps.initialTime;
        const viewModeChanged = this.props.viewMode !== nextProps.viewMode;
        const autoSelectChanged = this.props.autoSelect !== nextProps.autoSelect;
        return selectedTimeChanged || affixChanged || modeChanged || initialTimeChanged || viewModeChanged || autoSelectChanged;
    }

    componentWillMount() {
        const initialTime = this.assignedProps.initialTime;
        this.setState({
            selectedTime: initialTime,
            mode: "h"
        });
    }

    componentDidMount() {
        const clockElement: any = ReactDOM.findDOMNode(this.refs["mask"]);

        this.center = {
            x: clockElement.offsetWidth / 2,
            y: clockElement.offsetHeight / 2
        };

        this.basePoint = {
            x: this.center.x,
            y: 0
        };

        this.handleChangeMode(this.state.mode);
    }

    componentWillReceiveProps(nextProps: IClockProps) {
        const {initialTime} = nextProps;
        const {mode} = this.state;

        const angle = this.getAngleByTimeValue(initialTime, mode);
        this.setState({
            selectedTime: initialTime,
            mode: "h"
        }, () => this.handleChangeArrowAngle(angle));
    }

    setMode(mode: ClockMode) {
        this.setState({
            mode: mode
        } as IClockState);
    }

    getTime(): string {
        return this.state.selectedTime;
    }

    private handleSelectAffix(affix: Affix) {
        if (affix === getAffix(this.state.selectedTime)) {
            return;
        }

        const hours = moment(this.state.selectedTime).hours();
        let newHours = hours + 12;

        if (affix === "am") {
            newHours = hours % 13;
        }

        this.handleChangeHours(moment().hours(newHours).hours(), affix);
    };

    private handleChangeHours(time: number, finished: boolean | Affix | undefined) {
        const {selectedTime} = this.state;
        let affix: any;
        if (typeof finished === "string") {
            affix = finished;
            finished = undefined;
        }
        if (!affix) {
            affix = getAffix(selectedTime);
        }

        if (affix === "pm") {
            time = time + 12;
        } else {
            time = time % 12;
        }

        const newTime = moment(selectedTime).hours(time).format();
        if (newTime !== selectedTime) {
            this.setState({
                selectedTime: newTime
            } as IClockState);
        }

        if (finished) {
            setTimeout(() => {
                MUST_AUTOCHANGE_MODE && this.setState({
                    mode: "m"
                } as IClockState);
                this.props.onChangeHours && this.props.onChangeHours(newTime);
            }, 100);
        }
    }

    private handleChangeMinutes(time: number, finished: boolean = true) {
        const {selectedTime} = this.state;

        const hours = moment(selectedTime).hours();
        const newTime = moment(selectedTime).hours(hours).minutes(time).format();

        this.setState({
            selectedTime: newTime
        } as IClockState);

        if (this.props.onChangeMinutes && finished) {
            setTimeout(() => {
                this.props.onChangeMinutes(newTime);
            }, 0);
        }
    }

    private getAngleByTimeValue(selectedTime: string, mode: ClockMode) {
        const time = moment(selectedTime);
        let value: number;
        let step: number;
        if (mode as string === "h") {
            value = time.hours() % 12;
            step = 360 / 12;
        } else {
            value = time.minutes();
            step = 360 / 60;
        }
        return value * step;
    }

    private handleChangeMode(mode: ClockMode) {
        const {selectedTime} = this.state;
        const angle = this.getAngleByTimeValue(selectedTime, mode);
        this.setMode(mode);
        this.handleChangeArrowAngle(angle);
    }

    private handleChangeAffix(affix: Affix) {
        const {selectedTime} = this.state;
        if (affix !== getAffix(selectedTime)) {
            let hours = moment(selectedTime).hours();
            if (affix === "am") {
                hours = hours % 12;
            }
            this.handleChangeHours(hours, affix);
        }
    }

    private handleChangeArrowAngle(angle: number) {
        this.arrowAngle = angle;
        (this.refs["arrow"] as any).style.transform = `rotate(${angle}deg)`;
    }

    private getTimeByEvent(event: any) {
        const mode = this.state.mode;
        let items = 12;
        if (mode === "m") {
            items = 60;
        }
        const step = 360 / items;

        const {nativeEvent} = event;
        const ex = nativeEvent.offsetX;
        const ey = nativeEvent.offsetY;
        const center = {
            x: 150,
            y: 150
        };

        let x = ex - center.x;
        let y = (ey - center.y) * -1;
        let isXneg = false;
        let isYneg = false;

        if (x < 0) {
            isXneg = true;
            x = Math.abs(x);
        }
        if (y < 0) {
            isYneg = true;
            y = Math.abs(y);
        }

        const atan = Math.atan2(x, y);
        let deg = rad2deg(atan);

        deg = Math.round(deg / step) * step;
        deg %= 360;


        if (isYneg && isXneg) {
            deg += 180;
        } else if (isYneg) {
            deg = 180 - deg;
        } else if (isXneg) {
            deg = 360 - deg;
        }

        this.arrowAngle !== deg && this.handleChangeArrowAngle(deg);

        return (deg / step) % items;
    }

    private handleMouseMove(event: any) {
        event.preventDefault();
        if (isMousePressed(event)) {
            const value = this.getTimeByEvent(event);
            const selectedTime = moment(this.state.selectedTime);
            if (this.state.mode === "h") {
                selectedTime.hours() !== value && this.handleChangeHours(value, false);
            } else {
                selectedTime.minutes() !== value && this.handleChangeMinutes(value, false);
            }
        }
        return false;
    }

    private handleMouseUp(event: any) {
        event.preventDefault();
        const value = this.getTimeByEvent(event);
        if (this.state.mode === "h") {
            this.handleChangeHours(value, true);
            MUST_AUTOCHANGE_MODE && this.handleChangeMode("m");
        } else {
            this.handleChangeMinutes(value, true);
        }
        return;
    }

    private handleAcceptClick() {
        // if (this.state.mode === "m") {
            this.props.onAcceptClick && this.props.onAcceptClick();
        // } else {
        //     this.handleChangeMode("m");
        // }
    }

    render() {
        let clock: any;
        const {selectedTime, mode} = this.state;
        const momentSelected = moment(this.state.selectedTime);
        const {viewMode, autoSelect, cancelLabel, okLabel, onDismissClick} = this.props;
        const affix = getAffix(selectedTime);
        if (this.state.mode === "h") {
            clock = <ClockItems
                key="hours"
                ref="hours"
                type="hours"
                onChange={this.handleChangeHours}
                affix={affix}
                initialValue={momentSelected.hours() % 12}
                currentValue={moment().hours()}
                changeArrowAngle={this.handleChangeArrowAngle}
            />;
        } else {
            clock = <ClockItems
                key="minutes"
                ref="minutes"
                type="minutes"
                onChange={this.handleChangeMinutes}
                initialValue={momentSelected.minutes()}
                currentValue={moment().minutes()}
                changeArrowAngle={this.handleChangeArrowAngle}
            />;
        }

        return (
            <div className={classnames("clock-root", `clock-mode--${viewMode}`)}>
                <TimeDisplay
                    affix={affix}
                    mode={mode}
                    initialTime={selectedTime}
                    onSelectAffix={this.handleChangeAffix}
                    onSelectHour={() => this.handleChangeMode("h")}
                    onSelectMin={() => this.handleChangeMode("m")}
                />
                <div className="clock--face">
                    <div className="display-clock-item">
                        <div className="arrow" ref="arrow"/>
                        <div className="digts">
                            {clock}
                        </div>
                        <div className="clock-mask"
                             ref="mask"
                             onMouseMove={this.handleMouseMove}
                             onMouseUp={this.handleMouseUp}>
                        </div>
                    </div>
                    {!autoSelect &&
                    <div className="time-picker-actions">
                        <FlatButton
                            color="date-blue"
                            className="calendar-action-button--cancel"
                            onClick={onDismissClick}>
                            {cancelLabel}
                        </FlatButton>
                        <FlatButton
                            color="date-blue"
                            className="calendar-action-button--submit"
                            onClick={this.handleAcceptClick}>
                            {okLabel}
                        </FlatButton>
                    </div>
                    }
                </div>
            </div>
        );
    }
}