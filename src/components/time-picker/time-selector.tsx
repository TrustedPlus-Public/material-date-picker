import * as React from "react";
import { FlatButton } from "../button/index";
import { SlideInTransitionGroup, SlideDirection } from "../slideIn-transition-group/slideIn-transition-group";

interface ITimeSelectorProps {
    value: number;
    minValue: number;
    maxValue: number;
    onChange: (newValue: number) => void;
    tabIndex: number;
}

interface ITimeSelectorState {
    value: number;
    direction: SlideDirection;
}

export class TimeSelector extends React.Component<ITimeSelectorProps, ITimeSelectorState> {
    applyingValue: number;

    constructor(props: ITimeSelectorProps) {
        super(props);

        this.state = {
            value: props.value,
            direction: "up"
        };

        this.changeValue = this.changeValue.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleChangeValue = this.handleChangeValue.bind(this);
    }

    componentWillReceiveProps(nextProps: ITimeSelectorProps) {
        this.applyingValue = -1;
        this.setState({
            value: nextProps.value,
            direction: this.props.value < nextProps.value ? "up" : "down"
        });
    }

    changeValue(additionalValue: number) {
        const newValue = this.state.value + additionalValue;
        const { minValue, maxValue } = this.props;
        if (maxValue > -1 && minValue > -1) {
            if (newValue >= minValue && newValue <= maxValue) {
                this.props.onChange && this.props.onChange(newValue);
            }
        } else {
            this.props.onChange && this.props.onChange(newValue);
        }
        return false;
    }

    shouldComponentUpdate(nextProps: ITimeSelectorProps) {
        return this.state.value !== nextProps.value;
    }

    handleChangeValue(target: any) {
        const value = target.textContent * 1;
        let newValue = value;
        if (value > this.props.maxValue) {
            newValue = this.props.maxValue;
        } else if (value < this.props.minValue) {
            newValue = this.props.minValue;
        }
        setTimeout(() => {
            const value = target.textContent * 1;
            if (this.applyingValue === newValue) {
                this.props.onChange(newValue);
            } else {
                this.applyingValue = newValue;
                this.handleChangeValue(target);
            }
        }, 250);
    }

    handleKeyUp(event: any) {
        return false;
        // const {keyCode} = event;
        // if ([46, 8, 9, 27, 13, 110, 190].indexOf(keyCode) !== -1 ||
        //     (keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) ||
        //     (keyCode >= 35 && keyCode <= 40)) {
        //     return;
        // }
        // if ((event.shiftKey || (keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
        //     event.preventDefault();
        // } else {
        //     this.handleChangeValue(event.target);
        // }
    }

    render() {
        const text = this.state.value >= 10 ? `${this.state.value}` : `0${this.state.value}`;
        return (
            <div className="time-selector--root">
                <FlatButton onClick={() => {
                    this.changeValue(1);
                }}>
                    <i className="material-icons">expand_less</i>
                </FlatButton>
                <SlideInTransitionGroup direction={this.state.direction}>
                    <div className="time-selector--value" tabIndex={this.props.tabIndex || 0}
                        onKeyUpCapture={this.handleKeyUp}>
                        {text}
                    </div>
                </SlideInTransitionGroup>
                <FlatButton onClick={() => {
                    this.changeValue(-1);
                }}>
                    <i className="material-icons">expand_more</i>
                </FlatButton>
            </div>
        );
    }
}