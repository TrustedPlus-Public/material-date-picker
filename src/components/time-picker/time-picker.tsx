import * as React from "react";
import {InputField} from "../input/index";
import {assign} from "../core/object";

import {moment, DEFAULT_LABEL_OK, DEFAULT_LABEL_CANCEL} from "./index";
import {TimePickerDialog} from "./time-picker-dialog";

interface ITimePickerState {
    time: string;
}

export interface ITimePickerProps {
    autoSelect?: boolean;
    cancelLabel?: string;
    className?: string;
    defaultTime?: string;
    disabled?: boolean;
    okLabel?: string;
    onChange?: (date: string) => void;
    onDismiss?: () => void;
    onShow?: () => void;
    mode?: "portrait"|"landscape";
    ico?: string;
}

export class TimePicker extends React.Component<ITimePickerProps, ITimePickerState> {
    assignedProps: ITimePickerProps;

    static defaultProps: ITimePickerProps = {
        autoSelect: false,
        cancelLabel: DEFAULT_LABEL_CANCEL,
        defaultTime: moment().format(),
        disabled: false,
        okLabel: DEFAULT_LABEL_OK,
        onChange: (date: string) => void 0,
        onDismiss: () => void 0,
        onShow: () => void 0,
        mode: "portrait"
    };

    constructor(props: ITimePickerProps) {
        super(props);

        this.assignedProps = assign(TimePicker.defaultProps, props);

        this.state = {
            time: this.assignedProps.defaultTime as string
        };

        this.handleAccept = this.handleAccept.bind(this);
    }

    componentWillReceiveProps(nextProps: ITimePickerProps) {
        let newState: any = {};
        if (nextProps.defaultTime !== undefined && this.props.defaultTime !== nextProps.defaultTime) {
            newState.time = nextProps.defaultTime;
        }
        this.setState(newState);
    }

    componentWillMount() {
        this.setState({
            time: this.assignedProps.defaultTime as string
        });
    }

    private handleAccept(time: string) {
        this.setState({
            time: time
        });
        this.props.onChange && this.props.onChange(time);
    }

    render() {
        this.assignedProps = assign(TimePicker.defaultProps, this.props);

        const {
            autoSelect,
            cancelLabel,
            className,
            disabled,
            okLabel,
            onDismiss,
            onShow,
            mode,
            ico
        } = this.assignedProps;

        return (
            <div className={`time-picker-root ${className}`} tabIndex={2}>
                <InputField
                    ico={ico}
                    className="time-picker--input-field" disabled={disabled}
                    value={moment(this.state.time).format("LT")}
                    onClick={() => (this.refs["pickerDialog"] as TimePickerDialog).open()}
                    onChange={() => void 0}/>
                <TimePickerDialog
                    initialTime={this.state.time}
                    autoSelect={autoSelect as boolean}
                    cancelLabel={cancelLabel as string}
                    okLabel={okLabel as string}
                    onAccept={this.handleAccept}
                    onDismiss={onDismiss as () => void}
                    onShow={onShow as () => void}
                    mode={mode as "portrait" | "landscape"}
                    ref="pickerDialog"
                />
            </div>
        );
    }
}