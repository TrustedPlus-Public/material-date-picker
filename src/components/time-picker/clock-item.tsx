import * as React from "react";
import * as classnames from "classnames";
import { findDOMNode } from "react-dom";
import { Affix } from "./index";

function isMousePressed(event: any) {
    if (typeof event.buttons === "undefined") {
        return event.nativeEvent.which;
    }
    return event.buttons;
}

function removeClassName(element: Element, className: string) {
    element && element.classList.remove(className);
}

function addClassName(element: Element, className: string) {
    element && element.classList.add(className);
}

interface IClockItemsProps {
    type: "hours" | "minutes";
    onChange: (time: number, mustCallTimeChange?: boolean) => void;
    changeArrowAngle: (value: number) => void;
    initialValue: number;
    currentValue: number;
    affix?: Affix;
}

interface IClockItemsState {
}

interface IClockItem {
    label: string;
    isCurrent: boolean;
    isSelected: boolean;
    value: number;
}

export class ClockItems extends React.Component<IClockItemsProps, IClockItemsState> {
    shouldComponentUpdate(nextProps: IClockItemsProps) {
        const currentValueChanged = this.props.currentValue !== nextProps.currentValue;
        const typeChanged = this.props.type !== nextProps.type;
        const initialValueChanged = this.props.initialValue !== nextProps.initialValue;
        const affixChanged = (nextProps.type === "hours" && (nextProps.affix !== this.props.affix));
        return initialValueChanged || typeChanged || currentValueChanged || affixChanged;
    }

    private getItems(count: number) {
        const { initialValue, currentValue, type, affix } = this.props;
        let current = currentValue;
        if (type === "hours" && affix === "pm") {
            current = currentValue % 12;
        }
        let clockItems: IClockItem[] = [];
        for (let i = 0; i < count; i++) {
            const clockItem: IClockItem = {
                label: "" + i,
                isCurrent: current === i,
                isSelected: initialValue === i,
                value: i
            };
            if (type === "hours" && i === 0 && affix === "pm") {
                clockItem.label = "" + 12;
            }
            clockItems.push(clockItem);
        }
        return clockItems;
    }

    render() {
        const { type, onChange } = this.props;
        const count = this.props.type === "hours" ? 12 : 60;
        const items = this.getItems(count);

        return (
            <div className={`display-${type}`}>
                {items.map((item: IClockItem) => {
                    const { label, isCurrent, isSelected, value } = item;
                    const itemKey = `${type.slice(0, -1)}-${label}`;
                    const itemClassName = classnames(type, itemKey, {
                        "clock-item--current": isCurrent,
                        "clock-item--selected": isSelected
                    });

                    return (
                        <div className={itemClassName}
                            key={itemKey}
                            ref={itemKey}
                            data-value={value}
                            onMouseOver={(event: any) => isMousePressed(event) && onChange(value, false)}
                            onMouseUp={(event: any) => onChange(value)}
                        >
                            {label}
                        </div>
                    );
                })}
            </div>
        );
    }
}