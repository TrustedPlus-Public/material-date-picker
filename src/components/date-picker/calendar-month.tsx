import * as React from "react";
import * as classnames from "classnames";

import { moment, Moment } from "./index";
import { getFirstDayOfMonth, getLastDayOfMonth, isSameDate } from "../core/date-time-utils";
import { SlideInTransitionGroup, SlideDirection } from "../slideIn-transition-group";
import { DayButton } from "./day-button";
import { IWeekDayLabel } from "./calendar-days";

const DAYS_IN_WEEK = 7;


export interface IWeekDay extends IWeekDayLabel {
    isCurrentDate: boolean;
    isNeighborMonth: boolean;
    moment: Moment | undefined;
    isSelectedDate: boolean;
    element?: any;
}

export interface ICalendarMonthProps {
    autoSelect: boolean;
    direction: SlideDirection;
    displayDate: string;
    locale: string;
    maxDate: string;
    minDate: string;
    selectedDate: string;
    showNeighboringMonths: boolean;
    onDateSelect: (day: IWeekDay) => void;
}

export class CalendarMonth extends React.Component<ICalendarMonthProps, {}> {
    private lastSelectedDate: any;

    shouldComponentUpdate(nextProps: ICalendarMonthProps) {
        const { locale, displayDate, showNeighboringMonths } = this.props;
        let shouldUpdate = false;

        if (locale !== nextProps.locale) {
            shouldUpdate = true;
        }
        if (displayDate !== nextProps.displayDate) {
            shouldUpdate = true;
        }
        if (showNeighboringMonths !== nextProps.showNeighboringMonths) {
            shouldUpdate = true;
        }
        return shouldUpdate;
    }

    getDayElement(day: IWeekDay, key: string, showNeighboardMonth: boolean) {
        let element: any = <div key={key} className="calendar-week--day" />;
        if ((showNeighboardMonth && day.isNeighborMonth) || !day.isNeighborMonth) {
            element = <DayButton
                key={key}
                ref={key}
                className={classnames({
                    "day-current": day.isCurrentDate,
                    "day-weekend": day.isWeekend,
                    "month-neighbor": day.isNeighborMonth
                })}
                isSelected={day.isSelectedDate}
                onClick={() => {
                    this.props.onDateSelect(day);
                    const prevSelected = this.lastSelectedDate;
                    this.lastSelectedDate = key;
                    setTimeout(() => {
                        this.refs[prevSelected] && (this.refs[prevSelected] as any).unselect();
                        (this.refs[this.lastSelectedDate] as any).select();
                    }, 50);
                }}>
                {day.label}
            </DayButton>;
        }
        return element;
    }

    getWeekDays(): Array<IWeekDay[]> {
        const { props } = this;
        const { showNeighboringMonths } = props;
        const propsDisplayDate = props.displayDate;
        const selectedDate = props.selectedDate;

        const current = moment();
        const displayDate = moment(propsDisplayDate);

        const firstDate = getFirstDayOfMonth(propsDisplayDate);
        const lastDate = getLastDayOfMonth(propsDisplayDate);
        const firstIsoWeekday = moment(firstDate).weekday();
        const lastWeekday = moment(lastDate).weekday();

        const daysInMonth = displayDate.daysInMonth();
        const weeksCount = Math.ceil((daysInMonth + firstIsoWeekday) / DAYS_IN_WEEK);

        let monthDays: Array<IWeekDay[]> = [];
        let prevDate: any;

        for (let weekCounter = 0; weekCounter < weeksCount; weekCounter++) {
            const week: IWeekDay[] = monthDays[weekCounter] = [];
            if (weekCounter === 0) {
                prevDate = moment(firstDate).subtract(firstIsoWeekday + 1, "day");
            }

            for (let weekDay = 0; weekDay < DAYS_IN_WEEK; weekDay++) {
                (prevDate as Moment).add(1, "day");
                let day: IWeekDay = {
                    label: "",
                    isWeekend: false,
                    isCurrentDate: false,
                    isSelectedDate: false,
                    isNeighborMonth: false,
                    moment: (prevDate as Moment).clone()
                };

                if (((weekCounter === 0) && (weekDay < firstIsoWeekday)) ||
                    (((weekCounter === weeksCount - 1) && ((day.moment as Moment).weekday() > lastWeekday)))) {
                    day.isNeighborMonth = true;
                }

                const dayIsoWeekday = (day.moment as Moment).isoWeekday();
                day.isCurrentDate = isSameDate(current, day.moment as Moment);
                day.isSelectedDate = isSameDate(selectedDate, day.moment as Moment);
                day.isWeekend = (dayIsoWeekday === DAYS_IN_WEEK) || (dayIsoWeekday === DAYS_IN_WEEK - 1);

                day.label = "" + (day.moment as Moment).date();
                const key = `day-${weekDay}-of-${weekCounter}`;
                day.element = this.getDayElement(day, key, showNeighboringMonths);
                day.isSelectedDate && (this.lastSelectedDate = key);

                week.push(day);
            }
        }
        return monthDays;
    }

    render() {
        const weeksDays = this.getWeekDays();

        return (
            <SlideInTransitionGroup direction={this.props.direction} className="calendar-weeks">
                {weeksDays.map((days: IWeekDay[], weekIndex: number) => {
                    return (
                        <div
                            className="calendar-week"
                            key={`week-${weekIndex}`}>
                            {
                                days.map((day: IWeekDay) => {
                                    return day.element;
                                })
                            }
                        </div>
                    );
                })}
            </SlideInTransitionGroup>
        );
    }
}