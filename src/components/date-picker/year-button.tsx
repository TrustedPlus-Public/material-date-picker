import * as React from "react";
import * as classnames from "classnames";
import { FlatButton } from "../button/index";

interface IYearButtonProps {
    onClick: () => void;
    className?: string;
    key?: any;
}

export class YearButton extends React.Component<IYearButtonProps> {
    render() {
        return (
            <FlatButton onClick={this.props.onClick} className={classnames(this.props.className ? this.props.className : "", "year-button")} color="none">
                {this.props.children}
            </FlatButton>
        );
    }
}