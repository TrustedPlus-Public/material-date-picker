import * as React from "react";
import { moment, DIFFERENCE_IN_YEARS, DEFAULT_LABEL_CANCEL, DEFAULT_LOCALE, DEFAULT_LABEL_OK } from "./index";
import { InputField } from "../input/index";
import { DatePickerDialog } from "./date-picker-dialog";
import { assign } from "../core/object";
import { getUserLanguage } from "../core/date-time-utils";

interface IDatePickerState {
    date: string;
}

export interface IDatePickerProps {
    autoSelect?: boolean;
    cancelLabel?: string;
    className?: string;
    defaultDate?: string | Date;
    disableYearSelection?: boolean;
    disabled?: boolean;
    hideCalendarDate?: boolean;
    locale?: string;
    maxDate?: string;
    minDate?: string;
    mode?: "portrait" | "landscape";
    okLabel?: string;
    onChange?: (date: string) => void;
    onDismiss?: () => void;
    onShow?: () => void;
    placeholder?: string;
    showNeighboringMonths?: boolean;
    ico?: string;
}

export class DatePicker extends React.Component<IDatePickerProps, IDatePickerState> {
    static defaultProps: IDatePickerProps = {
        autoSelect: false,
        cancelLabel: DEFAULT_LABEL_CANCEL,
        className: "",
        defaultDate: moment().format(),
        disableYearSelection: false,
        disabled: false,
        hideCalendarDate: false,
        locale: DEFAULT_LOCALE || getUserLanguage(),
        mode: "landscape",
        okLabel: DEFAULT_LABEL_OK,
        showNeighboringMonths: true,
        maxDate: moment().add(DIFFERENCE_IN_YEARS, "year").format(),
        minDate: moment().subtract(DIFFERENCE_IN_YEARS, "year").format()
    };

    constructor(props: IDatePickerProps) {
        super(props);

        this.state = {
            date: moment(props.defaultDate).format() || moment().format()
        };

        this.getDate = this.getDate.bind(this);
        this.handleAccept = this.handleAccept.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
        this.handleInputClick = this.handleInputClick.bind(this);
    }

    componentWillReceiveProps(nextProps: IDatePickerProps) {
        if (this.props.locale !== nextProps.locale || moment.locale() !== nextProps.locale) {
            moment.locale(nextProps.locale);
        }
        let newState: any = {};
        if (nextProps.defaultDate !== undefined && this.props.defaultDate !== nextProps.defaultDate) {
            newState.date = nextProps.defaultDate;
        }
        this.setState(newState);
    }

    componentWillMount() {
        if (moment.locale() !== this.props.locale) {
            moment.locale(this.props.locale);
        }
    }

    getDate() {
        return this.state.date;
    }

    private handleAccept(date: string) {
        this.setState({
            date: date
        });
        this.props.onChange && this.props.onChange(date);
    }

    private handleDismiss() {
        this.props.onDismiss && this.props.onDismiss();
    }

    private handleInputClick() {
        (this.refs["pickerDialog"] as DatePickerDialog).open();
        this.props.onShow && this.props.onShow();
    }

    render() {
        const {
            autoSelect,
            className,
            disableYearSelection,
            disabled,
            formatDate,
            hideCalendarDate,
            locale,
            maxDate,
            minDate,
            mode,
            onShow,
            cancelLabel,
            okLabel,
            placeholder,
            showNeighboringMonths,
            ico
        } = assign(DatePicker.defaultProps, this.props);

        const { date } = this.state;

        return (
            <div className={`date-picker-root ${className}`}>
                <InputField placeholder={placeholder} disabled={disabled} value={moment(date).format("LL")}
                    ico={ico}
                    onClick={this.handleInputClick}
                    onChange={() => void 0} />
                <DatePickerDialog
                    autoSelect={autoSelect}
                    cancelLabel={cancelLabel}
                    disableYearSelection={disableYearSelection}
                    hideCalendarDate={hideCalendarDate}
                    locale={locale}
                    maxDate={maxDate}
                    minDate={minDate}
                    mode={mode}
                    formatDate={formatDate}
                    okLabel={okLabel}
                    onAccept={this.handleAccept}
                    onDismiss={this.handleDismiss}
                    onShow={onShow}
                    onChangeLocale={() => void 0}
                    onChangeShowNeighboringMonths={() => void 0}
                    initialDate={date}
                    showNeighboringMonths={showNeighboringMonths}
                    ref="pickerDialog"
                />
            </div>
        );
    }
}