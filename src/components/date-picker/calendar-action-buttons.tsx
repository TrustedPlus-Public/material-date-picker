import * as React from "react";

import { FlatButton } from "../button/index";
import { EventHandler } from "react";

export interface ICalendarActionButtonsProps {
    cancelLabel: string;
    submitLabel: string;
    cancelTitle?: string;
    submitTitle?: string;
    onCancelClick: (event: any) => void;
    onSubmitClick: (event: any) => void;
}

interface ICalendarActionButtonsState {

}

export class CalendarActionButtons extends React.Component<ICalendarActionButtonsProps, ICalendarActionButtonsState> {
    render() {
        const {
            cancelTitle,
            cancelLabel,
            submitLabel,
            submitTitle,
            onCancelClick,
            onSubmitClick
        } = this.props;
        return (
            <div className="calendar-action-buttons">
                <FlatButton
                    color="date-blue"
                    className="calendar-action-button--cancel"
                    title={cancelTitle}
                    onClick={onCancelClick}
                >
                    {cancelLabel}
                </FlatButton>
                <FlatButton
                    className="calendar-action-button--submit"
                    color="date-blue"
                    title={submitTitle}
                    onClick={onSubmitClick}>
                    {submitLabel}
                </FlatButton>
            </div>
        );
    }
}