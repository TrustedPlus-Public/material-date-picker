import * as React from "react";
import * as classnames from "classnames";

import { moment } from "./index";
import { SlideInTransitionGroup, SlideDirection } from "../slideIn-transition-group";

interface IDateDisplayProps {
    locale: string;
    disableYearSelection: boolean;
    monthDaySelected: boolean;
    selectedDate: string;
    onYearClick: () => void;
    onMonthDayClick: () => void;
}

interface IDateDisplayState {
    transitionDirection: SlideDirection;
    monthDaySelected: boolean;
}

export class DateDisplay extends React.Component<IDateDisplayProps, IDateDisplayState> {
    constructor(props: IDateDisplayProps) {
        super(props);

        this.state = {
            transitionDirection: "up",
            monthDaySelected: true
        };

        this.handleYearClick = this.handleYearClick.bind(this);
        this.handleMonthDayClick = this.handleMonthDayClick.bind(this);
    }

    componentWillMount() {
        const { monthDaySelected } = this.props;
        if (this.state.monthDaySelected !== monthDaySelected) {
            this.setState({ monthDaySelected: monthDaySelected } as IDateDisplayState);
        }
    }

    componentWillReceiveProps(nextProps: IDateDisplayProps) {
        let newState: any = {};
        let mustUpdateState = false;

        const { selectedDate } = this.props;
        if (nextProps.selectedDate !== selectedDate) {
            newState.transitionDirection = moment(nextProps.selectedDate).isAfter(moment(selectedDate)) ? "up" : "down";
            mustUpdateState = true;
        }

        if (nextProps.monthDaySelected !== undefined && nextProps.monthDaySelected !== this.state.monthDaySelected) {
            newState.monthDaySelected = nextProps.monthDaySelected;
            mustUpdateState = true;
        }
        mustUpdateState && this.setState(newState);
    }

    shouldComponentUpdate(nextProps: IDateDisplayProps) {
        let shouldUpdate = false;
        const { monthDaySelected, selectedDate, locale } = this.props;
        if (nextProps.selectedDate !== selectedDate) {
            shouldUpdate = true;
        }
        if (nextProps.monthDaySelected !== undefined && nextProps.monthDaySelected !== monthDaySelected) {
            shouldUpdate = true;
        }
        if (nextProps.locale !== locale) {
            shouldUpdate = true;
        }
        return shouldUpdate;
    }

    handleYearClick() {
        if (this.props.onYearClick && !this.props.disableYearSelection && this.state.monthDaySelected) {
            this.props.onYearClick();
        }
    }

    handleMonthDayClick() {
        if (this.props.onMonthDayClick && !this.state.monthDaySelected) {
            this.props.onMonthDayClick();
        }
    }

    render() {
        const { selectedDate } = this.props;
        const { monthDaySelected } = this.state;

        const date = moment(selectedDate);
        const year = moment(date).format("YYYY");
        const dateTimeFormatted = date.format("llll");
        const dateTimeLabel = dateTimeFormatted.slice(0, dateTimeFormatted.indexOf(date.year().toString()) - 1);

        return (
            <div className="calendar-date-display--root">
                <div
                    className={classnames("calendar-display--year-title", "calendar-display-date--title", { "calendar-date-display--title-active": !monthDaySelected })}>
                    <DisplayYear
                        direction={this.state.transitionDirection}
                        onClick={this.handleYearClick}
                        year={year} />
                </div>
                <div
                    className={classnames("calendar-display--date-month-title calendar-display-date--title", { "calendar-date-display--title-active": monthDaySelected })}
                    onClick={this.handleMonthDayClick}>
                    {dateTimeLabel}
                </div>
            </div>
        );
    }
}


interface IDisplayYearProps {
    year: string;
    direction: SlideDirection;
    onClick: () => void;
}

class DisplayYear extends React.Component<IDisplayYearProps, {}> {
    shouldComponentUpdate(nextProps: IDisplayYearProps) {
        return this.props.year !== nextProps.year;
    }

    render() {
        const { year, direction, onClick } = this.props;
        return (
            <SlideInTransitionGroup direction={direction}>
                <div key={`year-${year}`}
                    className="date-display--label-year"
                    onClick={onClick}>
                    {year}
                </div>
            </SlideInTransitionGroup>
        );
    }
}
