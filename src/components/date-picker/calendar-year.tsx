import * as React from "react";
import { findDOMNode } from "react-dom";
import * as classnames from "classnames";

import { moment } from "./index";
import { YearButton } from "./year-button";

export interface ICalendarYearProps {
    key?: string;
    onYearClick: (year: any) => void;
    selectedDate: string;
    minDate: string;
    maxDate: string;
    mode?: any;
}

export class CalendarYear extends React.Component<ICalendarYearProps> {
    componentDidMount() {
        this.scrollToSelectedYear();
    }

    componentDidUpdate() {
        this.scrollToSelectedYear();
    }

    getYearElement(year: any, handleClick: (year: any) => void) {
        const key = `yb${year.label}`;
        return (
            <YearButton
                key={key}
                ref={key}
                onClick={() => handleClick(year)}
                className={classnames("calendar-years--year", {
                    "year-selected": year.isSelected,
                    "year-current": year.isCurrent
                })}
            >
                {year.label}
            </YearButton>
        );
    }

    getYears() {
        const {
            minDate,
            maxDate,
            selectedDate,
        } = this.props;

        const minYear = moment(minDate).year();
        const maxYear = moment(maxDate).year();
        const years: any[] = [];
        const currentYear = moment().year();
        const selectedYear = moment(selectedDate).year();

        for (let i = minYear; i < maxYear; i++) {
            let year: any = {
                isSelected: i === selectedYear,
                isCurrent: i === currentYear,
                label: i
            };
            year.element = this.getYearElement(year, this.props.onYearClick);
            years.push(year);
        }

        return years;
    }

    scrollToSelectedYear() {
        const container: any = findDOMNode(this);
        const yearButtonNode: any = findDOMNode((document.querySelectorAll(".year-selected")[0]).parentNode as any);

        const containerHeight = container.clientHeight;
        const yearButtonNodeHeight = yearButtonNode.clientHeight || 32;

        const scrollYOffset = (yearButtonNode.offsetTop + yearButtonNodeHeight / 2) - containerHeight / 2;
        container.scrollTop = scrollYOffset;
    }

    render() {
        const years = this.getYears();
        return (
            <div className="calendar-year--root">
                {
                    years.map((year: any) => {
                        return year.element;
                    })
                }
            </div>
        );
    }
}