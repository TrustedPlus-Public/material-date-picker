import * as React from "react";
import * as classnames from "classnames";
import { getFirstWeekday } from "../core/date-time-utils";

function getNextDay(current: any, additionalDay: number = 1) {
    return current.add(additionalDay, "day");
}

export interface IWeekDayLabel {
    label: string;
    isWeekend: boolean;
}

function getDaysOfWeek(format: string) {
    let days: IWeekDayLabel[] = [];
    for (let i = 0; i < 7; i++) {
        const firstWeekday = getFirstWeekday();
        const next = getNextDay(firstWeekday.date, i);
        const weekday = firstWeekday.sundayWeekday + 7;
        days.push({
            label: next.format(format),
            isWeekend: ((i === (weekday % 7) || i === (weekday - 1) % 7))
        });
    }
    return days;
}

export interface ICalendarDaysProps {
    format: string;
    locale: string;
}

export class CalendarDaysOfWeek extends React.Component<ICalendarDaysProps, {}> {
    shouldComponentUpdate(nextProps: ICalendarDaysProps) {
        const { format, locale } = this.props;
        return !!((format && (format !== nextProps.format)) || locale !== nextProps.locale);
    }

    render() {
        const days = getDaysOfWeek(this.props.format);
        return (
            <div className="calendar-week--days">
                {days.map((day: IWeekDayLabel) => {
                    const { label, isWeekend } = day;
                    return <div
                        key={label}
                        className={classnames("calendar-week--day day-name", { "day-weekend": isWeekend })}>
                        {label}
                    </div>
                })}
            </div>
        )
    }
}