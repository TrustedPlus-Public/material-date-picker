import * as React from "react";
import * as classnames from "classnames";

import { Calendar } from "./calendar";
import { Mode } from "./index";

interface IDatePickerDialogProps {
    autoSelect: boolean;
    cancelLabel: string;
    disableYearSelection: boolean;
    formatDate: string;
    hideCalendarDate: boolean;
    showNeighboringMonths: boolean;
    locale: string;
    maxDate: string;
    minDate: string;
    mode: Mode;
    okLabel: string;
    onChangeLocale: (locale: string) => void;
    onChangeShowNeighboringMonths: (show: boolean) => void;
    onAccept: (date: string) => void;
    onDismiss: () => void;
    onShow: () => void;
    initialDate: string;
}

interface IDatePickerDialogState {
    isOpened: boolean;
}

export class DatePickerDialog extends React.Component<IDatePickerDialogProps, IDatePickerDialogState> {
    private opened: boolean = false;

    constructor(props: IDatePickerDialogProps) {
        super(props);

        this.state = {
            isOpened: false
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.handleDateSelect = this.handleDateSelect.bind(this);
        this.handleAccept = this.handleAccept.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
        this.handleWindowKeyUp = this.handleWindowKeyUp.bind(this);
        this.handleWrapperClick = this.handleWrapperClick.bind(this);
    }

    private handleDateSelect() {
        if (this.props.autoSelect) {
            setTimeout(this.handleAccept, 300);
        }
    }

    componentDidUpdate(nextProps: any, nextState: any) {
        if (this.opened !== nextState.isOpened) {
            nextState.isOpened = this.opened;
            this.setState(nextState);
        }
    }

    private handleAccept() {
        const date = (this.refs["calendar"] as Calendar).getSelectedDate();
        this.props.onAccept && this.props.onAccept(date);
        this.close();
    }

    private handleDismiss() {
        this.props.onDismiss && this.props.onDismiss();
        this.close();
    }

    private handleWrapperClick(event: any) {
        const { target } = event;
        if (target.classList.contains("do-close")) {
            this.close();
        }
    }

    private handleWindowKeyUp(event: any) {
        const { keyCode } = event;
        const calendar: any = this.refs["calendar"];
        switch (keyCode) {
            case 13:
                this.handleAccept();
                break;
            case 27:
                this.handleDismiss();
                break;
            default:
                calendar.handleKeyDown(event);
                break;
        }
        event.preventDefault();
        return false;
    }

    open() {
        this.opened = true;
        this.setState({
            isOpened: true
        });
    }

    close() {
        this.opened = false;
        this.setState({
            isOpened: false
        });
    }

    render() {
        const {
            autoSelect,
            cancelLabel,
            hideCalendarDate,
            disableYearSelection,
            initialDate,
            locale,
            showNeighboringMonths,
            maxDate,
            minDate,
            okLabel,
            mode
        } = this.props;

        const rootClassName = classnames("date-picker-dialog-root", { opened: this.state.isOpened });

        return (
            <div className={rootClassName}>
                <div className="date-picker-dialog-wrapper do-close"
                    tabIndex={1} ref="root"
                    onClick={this.handleWrapperClick}
                    onKeyUp={this.handleWindowKeyUp}>

                    <div className="date-picker-dialog-content" onClick={(event: any) => event.stopPropagation()}>

                        <Calendar
                            ref="calendar"
                            hideCalendarDate={hideCalendarDate}
                            autoSelect={autoSelect}
                            cancelLabel={cancelLabel}
                            disableYearSelection={disableYearSelection}
                            initialDate={initialDate}
                            locale={locale}
                            showNeighboringMonths={showNeighboringMonths}
                            onDayClick={this.handleDateSelect}
                            onAccept={this.handleAccept}
                            onDismiss={this.handleDismiss}
                            maxDate={maxDate}
                            minDate={minDate}
                            okLabel={okLabel}
                            mode={mode} />
                    </div>
                </div>
            </div>
        );
    }
}