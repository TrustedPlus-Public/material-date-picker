import * as React from "react";
import { IconButton } from "../button/index";
import { SlideInTransitionGroup, SlideDirection } from "../slideIn-transition-group";

import { moment } from "./index";

export interface ICalendarToolbarProps {
    displayDate: string;
    onMonthChange: (dir: number) => void;
    nextMonth: boolean;
    prevMonth: boolean;
    locale: string;
    direction: SlideDirection;
}

interface ICalendarToolbarState {
}

export class CalendarToolbar extends React.Component<ICalendarToolbarProps, ICalendarToolbarState> {
    constructor(props: ICalendarToolbarProps) {
        super(props);

        this.handleClickPrevMonth = this.handleClickPrevMonth.bind(this);
        this.handleClickNextMonth = this.handleClickNextMonth.bind(this);
    }

    shouldComponentUpdate(nextProps: ICalendarToolbarProps) {
        let shouldUpdate = false;
        const { locale, displayDate } = this.props;
        if (locale !== nextProps.locale || displayDate !== nextProps.displayDate) {
            shouldUpdate = true;
        }

        return shouldUpdate;
    }

    handleClickPrevMonth() {
        this.props.onMonthChange(-1);
    }

    handleClickNextMonth() {
        this.props.onMonthChange(1);
    }

    render() {
        const { displayDate, direction } = this.props;
        const dateFormatted = moment(displayDate).format("MMMM YYYY");

        return (
            <div className="calendar-toolbar--root">
                <IconButton
                    icon="keyboard_arrow_left"
                    className="calendar-toolbar--icon icon-left"
                    onClick={this.handleClickPrevMonth} />
                <div className="calendar-toolbar--label-container">
                    <SlideInTransitionGroup direction={direction}>
                        <div className={"calendar-toolbar--label"}>
                            {dateFormatted}
                        </div>
                    </SlideInTransitionGroup>
                </div>
                <IconButton
                    icon="keyboard_arrow_right"
                    className="calendar-toolbar--icon icon-right"
                    onClick={this.handleClickNextMonth} />
            </div>
        );
    }
}