import * as React from "react";
import * as classnames from "classnames";

import { assign } from "../core/object";
import { getFirstDayOfMonth, getUserLanguage, isBeforeDate, isAfterDate } from "../core/date-time-utils";

import { DateDisplay } from "./date-display";
import { CalendarToolbar } from "./calendar-toolbar";
import { CalendarDaysOfWeek } from "./calendar-days";
import { CalendarMonth, IWeekDay } from "./calendar-month";
import { CalendarYear } from "./calendar-year";
import { CalendarActionButtons } from "./calendar-action-buttons";
import { SlideDirection } from "../slideIn-transition-group";

import { moment, Mode, DIFFERENCE_IN_YEARS, DEFAULT_LABEL_CANCEL, DEFAULT_LABEL_OK } from "./index";

interface ICalendarProps {
    autoSelect?: boolean;
    cancelLabel?: string;
    disableYearSelection?: boolean;
    hideCalendarDate?: boolean;
    initialDate?: string;
    locale?: string;
    showNeighboringMonths?: boolean;
    onDayClick?: () => void;
    onAccept?: (event: any) => void;
    onDismiss?: (event: any) => void;
    maxDate?: string;
    minDate?: string;
    okLabel?: string;
    mode?: Mode;
}

interface ICalendarState {
    displayDate: string;
    selectedDate: string;
    displayMonthDay: boolean;
    transitionDirection: SlideDirection;
}

export class Calendar extends React.Component<ICalendarProps, ICalendarState> {
    static defaultProps: ICalendarProps = {
        autoSelect: false,
        cancelLabel: DEFAULT_LABEL_CANCEL,
        disableYearSelection: false,
        hideCalendarDate: false,
        initialDate: moment().format(),
        locale: getUserLanguage(),
        showNeighboringMonths: false,
        onDayClick: () => void 0,
        onAccept: () => void 0,
        onDismiss: () => void 0,
        maxDate: moment().add(DIFFERENCE_IN_YEARS, "year").format(),
        minDate: moment().subtract(DIFFERENCE_IN_YEARS, "year").format(),
        okLabel: DEFAULT_LABEL_OK,
        mode: "landscape"
    };

    protected assignedProps: ICalendarProps;

    constructor(props: ICalendarProps) {
        super(props);

        this.assignedProps = assign(Calendar.defaultProps, props);

        const locale = this.assignedProps.locale;
        moment.locale(locale);

        const initialDate = props.initialDate || "";
        const selectedDate = moment(initialDate).format();

        this.state = {
            displayDate: selectedDate,
            selectedDate: selectedDate,
            displayMonthDay: true,
            transitionDirection: "left"
        };

        this.handleMonthChange = this.handleMonthChange.bind(this);
        this.handleSelectDate = this.handleSelectDate.bind(this);
        this.handleDateDisplayMonthClick = this.handleDateDisplayMonthClick.bind(this);
        this.handleDateDisplayYearClick = this.handleDateDisplayYearClick.bind(this);
        this.handleYearSelect = this.handleYearSelect.bind(this);
        this.getSelectedDate = this.getSelectedDate.bind(this);
    }

    componentWillMount() {
        const { initialDate } = this.assignedProps;

        this.setState({
            displayDate: getFirstDayOfMonth(initialDate as string),
            selectedDate: initialDate as string
        } as ICalendarState);
    }

    componentWillReceiveProps(nextProps: ICalendarProps) {
        let mustUpdateState = false;
        let newState = {};

        if (this.assignedProps.locale !== nextProps.locale || moment.locale() !== nextProps.locale) {
            moment.locale(nextProps.locale);
            mustUpdateState = true;
        }

        const date = nextProps.initialDate || moment().format();
        const firstDay = getFirstDayOfMonth(date);
        if (this.state.selectedDate !== nextProps.initialDate || this.props.initialDate !== nextProps.initialDate || this.state.displayDate !== firstDay) {
            mustUpdateState = true;
            newState = {
                displayDate: firstDay,
                selectedDate: date
            };
        }

        mustUpdateState && this.setState(newState as ICalendarState);
    }

    private setDisplayDate(date: string, newSelectedDate: string) {
        const newDisplayDate = getFirstDayOfMonth(date);
        const direction = newDisplayDate > this.state.displayDate ? "left" : "right";

        if (newDisplayDate !== this.state.displayDate) {
            this.setState({
                displayDate: newDisplayDate,
                transitionDirection: direction,
                selectedDate: newSelectedDate || this.state.selectedDate
            } as ICalendarState);
        }
    }

    private setSelectedDate(date: string) {
        let adjustDate: string = date;
        const { minDate, maxDate } = this.assignedProps;
        if (isBeforeDate(date, minDate as string)) {
            adjustDate = minDate as string;
        } else if (isAfterDate(date, maxDate as string)) {
            adjustDate = maxDate as string;
        }

        const newDisplayDate = getFirstDayOfMonth(adjustDate);
        if (newDisplayDate !== this.state.displayDate) {
            this.setDisplayDate(newDisplayDate, adjustDate);
        } else {
            this.setState({
                selectedDate: adjustDate
            } as ICalendarState);
        }
    }

    getSelectedDate() {
        return this.state.selectedDate;
    }


    private handleMonthChange(month: number) {
        let displayDate: any;
        let direction: string = "left";
        if (month >= 0) {
            displayDate = moment(this.state.displayDate).add(1, "month");
            direction = "right";
        } else {
            displayDate = moment(this.state.displayDate).subtract(1, "month");
        }
        this.setState({
            transitionDirection: direction,
            displayDate: displayDate.format()
        } as ICalendarState);
    }

    private getToolbarInteractions() {
        const assignedProps = assign(Calendar.defaultProps, this.props);
        return {
            prevMonth: moment(this.state.displayDate).subtract(1, "month").isAfter(assignedProps.minDate, "month"),
            nextMonth: moment(this.state.displayDate).add(1, "month").isBefore(assignedProps.maxDate, "month")
        };
    }

    private changeDisplayDate(event: string, value: string) {
        const assignedProps = assign(Calendar.defaultProps, this.props);
        const date = moment(this.state.displayDate) as any;
        const newDate = date[event](1, value);
        if (newDate.isBetween(assignedProps.minDate, assignedProps.maxDate)) {
            this.setState({
                displayDate: newDate.format()
            } as ICalendarState);
        }
    }

    private handleKeyDown(event: any) {
        const code = event.keyCode;
        switch (code) {
            // left arrow
            case 37:
                this.changeDisplayDate("subtract", "month");
                event.preventDefault();
                break;
            // up arrow
            case 38:
                this.changeDisplayDate("subtract", "year");
                event.preventDefault();
                break;
            // right arrow
            case 39:
                this.changeDisplayDate("add", "month");
                event.preventDefault();
                break;
            // down arrow
            case 40:
                this.changeDisplayDate("add", "year");
                event.preventDefault();
                break;
            default:
                break;
        }
    }


    private handleSelectDate(date: IWeekDay) {
        date.moment && this.setSelectedDate(date.moment.format());
        this.props.onDayClick && this.props.onDayClick();
    }

    private handleYearSelect(year: any) {
        const selectedDate = moment(this.state.selectedDate).year(year.label);
        this.setState({ displayMonthDay: true } as ICalendarState);
        this.setSelectedDate(selectedDate.format());
    }

    private handleDateDisplayYearClick() {
        this.setState({
            displayMonthDay: false
        } as ICalendarState);
    }

    private handleDateDisplayMonthClick() {
        this.setState({
            displayMonthDay: true
        } as ICalendarState);
    }

    render() {
        const {
            disableYearSelection,
            hideCalendarDate,
            maxDate,
            minDate,
            mode,
            showNeighboringMonths,
            okLabel,
            cancelLabel,
            onAccept,
            onDismiss,
            autoSelect,
            locale
        } = this.assignedProps;

        const {
            displayDate,
            selectedDate,
            displayMonthDay,
            transitionDirection
        } = this.state;

        const toolbarInteractions = this.getToolbarInteractions();

        return (
            <div className="calendar">
                <div onKeyDown={this.handleKeyDown.bind(this)}
                    className="event-listener"
                    tabIndex={1} />
                <div className={classnames("calendar-root", `calendar-mode-${mode}`)}>


                    {!hideCalendarDate &&
                        <DateDisplay
                            locale={locale as string}
                            disableYearSelection={disableYearSelection as boolean}
                            onYearClick={this.handleDateDisplayYearClick}
                            onMonthDayClick={this.handleDateDisplayMonthClick}
                            selectedDate={selectedDate}
                            monthDaySelected={displayMonthDay} />
                    }

                    <div className="calendar-container">
                        <div className="calendar-content">
                            {displayMonthDay &&
                                <div>
                                    <CalendarToolbar
                                        displayDate={displayDate}
                                        onMonthChange={this.handleMonthChange}
                                        prevMonth={toolbarInteractions.prevMonth}
                                        nextMonth={toolbarInteractions.nextMonth}
                                        locale={locale as string}
                                        direction={this.state.transitionDirection} />

                                    <div className="calendar-weeks--root">
                                        <CalendarDaysOfWeek locale={locale as string} format="dd" />
                                        <CalendarMonth
                                            autoSelect={autoSelect as boolean}
                                            displayDate={moment(displayDate).startOf("month").format()}
                                            selectedDate={selectedDate}
                                            minDate={minDate as string}
                                            maxDate={maxDate as string}
                                            showNeighboringMonths={showNeighboringMonths as boolean}
                                            onDateSelect={this.handleSelectDate}
                                            locale={locale as string}
                                            direction={transitionDirection} />
                                    </div>
                                </div>
                            }
                            {!displayMonthDay && !disableYearSelection &&
                                <CalendarYear
                                    key="years"
                                    onYearClick={this.handleYearSelect}
                                    selectedDate={selectedDate}
                                    minDate={minDate as string}
                                    maxDate={maxDate as string} />
                            }
                        </div>
                        {this.state.displayMonthDay && !autoSelect &&
                            <CalendarActionButtons
                                submitLabel={okLabel as string}
                                cancelLabel={cancelLabel as string}
                                onSubmitClick={onAccept as (event: any) => void}
                                onCancelClick={onDismiss as (event: any) => void}
                            />}
                    </div>
                </div>
            </div>
        );
    }
}