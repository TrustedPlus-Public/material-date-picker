import * as React from "react";
import * as classnames from "classnames";

export interface IDayButtonProps {
    disabled?: boolean;
    className?: string;
    onClick: React.MouseEventHandler<any>;
    isSelected: boolean;
}

export class DayButton extends React.Component<IDayButtonProps, any> {
    constructor(props: IDayButtonProps) {
        super(props);

        this.state = {
            isSelected: props.isSelected || false
        };
    }

    select() {
        this.setState({
            isSelected: true
        });
    }

    unselect() {
        this.setState({
            isSelected: false
        });
    }

    render() {
        return (
            <div
                className={`calendar-week--day ${this.props.className} ${classnames({ "day-selected": this.state.isSelected, "day-disabled": this.props.disabled })}`}>
                {this.props.children
                    ?
                    <div className="day-button"
                        onClick={(event: any) => {
                            !this.props.disabled && this.props.onClick(event);
                        }}>
                        {this.props.children}
                    </div>
                    : ""}
            </div>
        );
    }
}