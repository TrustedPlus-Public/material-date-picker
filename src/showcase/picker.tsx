import * as React from "react";

// Components
import { DatePicker } from "../components";
import { TimePicker } from "../components/time-picker";
import {
    moment,
    DEFAULT_LABEL_CANCEL,
    DEFAULT_LABEL_OK,
    DEFAULT_LOCALE,
    DIFFERENCE_IN_YEARS,
    supportedLocales
} from "../components/date-picker/index";
import { getUserLanguage } from "../components/core/date-time-utils";


export class PickerDemo extends React.Component<{}, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            DPautoSelect: false,
            DPcancelLabel: DEFAULT_LABEL_CANCEL,
            DPdefaultDate: moment().format(),
            DPdisableYearSelection: false,
            DPdisabled: false,
            DPhideCalendarDate: false,
            DPlocale: DEFAULT_LOCALE || getUserLanguage(),
            DPmode: "portrait",
            DPokLabel: DEFAULT_LABEL_OK,
            DPshowNeighboringMonths: false,
            DPmaxDate: moment().add(DIFFERENCE_IN_YEARS, "year").format(),
            DPminDate: moment().subtract(DIFFERENCE_IN_YEARS, "year").format(),

            TPautoSelect: false,
            TPcancelLabel: DEFAULT_LABEL_CANCEL,
            TPdefaultTime: moment().format(),
            TPdisabled: false,
            TPokLabel: DEFAULT_LABEL_OK,
            TPmode: "portrait"
        };
    }

    render() {
        return (
            <div>
                <div>
                    <div>
                        <h3>Settings</h3>
                        <div className="settings-item">
                            <label>Auto select</label>
                            <input type="checkbox" checked={this.state.DPautoSelect}
                                onChange={() => this.setState({ DPautoSelect: !this.state.DPautoSelect })} />
                        </div>
                        <div className="settings-item">
                            <label>Ok label</label>
                            <input type="text" defaultValue={this.state.DPokLabel} onChange={(event: any) => {
                                const { target } = event;
                                const { value } = target;
                                this.setState({
                                    DPokLabel: value
                                });
                            }} />
                        </div>
                        <div className="settings-item">
                            <label>Cancel label</label>
                            <input type="text" defaultValue={this.state.DPcancelLabel} onChange={(event: any) => {
                                const { target } = event;
                                const { value } = target;
                                this.setState({
                                    DPcancelLabel: value
                                });
                            }} />
                        </div>
                        <div className="settings-item">
                            <label>Default date</label>
                            <input type="text" defaultValue={this.state.DPdefaultDate} onChange={(event: any) => {
                                const { target } = event;
                                const { value } = target;
                                this.setState({
                                    DPdefaultDate: value
                                });
                            }} />
                        </div>
                        <div className="settings-item">
                            <label>Disable year selection</label>
                            <input type="checkbox" checked={this.state.DPdisableYearSelection}
                                onChange={() => this.setState({ DPdisableYearSelection: !this.state.DPdisableYearSelection })} />
                        </div>
                        <div className="settings-item">
                            <label>Hide calendar date</label>
                            <input type="checkbox" checked={this.state.DPhideCalendarDate}
                                onChange={() => this.setState({ DPhideCalendarDate: !this.state.DPhideCalendarDate })} />
                        </div>
                        <div className="settings-item">
                            <label>Disabled</label>
                            <input type="checkbox" checked={this.state.DPdisabled}
                                onChange={() => this.setState({ DPdisabled: !this.state.DPdisabled })} />
                        </div>
                        <div className="settings-item">
                            <label>Locale</label>
                            <select value={this.state.DPlocale}
                                onChange={(event: any) => this.setState({ DPlocale: event.target.value })}>
                                {supportedLocales.map((lang: { value: string, label: string }) =>
                                    <option value={lang.value} key={lang.value}>{lang.label}</option>
                                )}
                            </select>
                        </div>

                        <div className="settings-item">
                            <label>Landscape mode</label>
                            <input type="checkbox" checked={(this.state.DPmode as string) === "landscape"}
                                onChange={() => this.setState({ DPmode: (this.state.DPmode as string) === "landscape" ? "portrait" : "landscape" })} />
                        </div>

                        <div className="settings-item">
                            <label>Show neighboring months</label>
                            <input type="checkbox" checked={this.state.DPshowNeighboringMonths}
                                onChange={() => this.setState({ DPshowNeighboringMonths: !this.state.DPshowNeighboringMonths })} />
                        </div>
                    </div>

                    <DatePicker
                        autoSelect={this.state.DPautoSelect}
                        cancelLabel={this.state.DPcancelLabel}
                        defaultDate={this.state.DPdefaultDate}
                        disableYearSelection={this.state.DPdisableYearSelection}
                        disabled={this.state.DPdisabled}
                        hideCalendarDate={this.state.DPhideCalendarDate}
                        locale={this.state.DPlocale}
                        maxDate={this.state.DPmaxDate}
                        minDate={this.state.DPminDate}
                        mode={this.state.DPmode}
                        okLabel={this.state.DPokLabel}
                        onChange={() => console.log("change")}
                        onDismiss={() => console.log("dismiss")}
                        onShow={() => console.log("show")}
                        showNeighboringMonths={this.state.DPshowNeighboringMonths}
                    />
                </div>
                <div>
                    <div>
                        <h3>Settings</h3>
                        <div className="settings-item">
                            <label>Auto select</label>
                            <input type="checkbox" checked={this.state.TPautoSelect}
                                onChange={() => this.setState({ TPautoSelect: !this.state.TPautoSelect })} />
                        </div>
                        <div className="settings-item">
                            <label>Ok label</label>
                            <input type="text" defaultValue={this.state.TPokLabel} onChange={(event: any) => {
                                const { target } = event;
                                const { value } = target;
                                this.setState({
                                    TPokLabel: value
                                });
                            }} />
                        </div>
                        <div className="settings-item">
                            <label>Cancel label</label>
                            <input type="text" defaultValue={this.state.TPcancelLabel} onChange={(event: any) => {
                                const { target } = event;
                                const { value } = target;
                                this.setState({
                                    TPcancelLabel: value
                                });
                            }} />
                        </div>
                        <div className="settings-item">
                            <label>Default time</label>
                            <input type="text" defaultValue={this.state.TPdefaultTime} onChange={(event: any) => {
                                const { target } = event;
                                const { value } = target;
                                this.setState({
                                    TPdefaultTime: value
                                });
                            }} />
                        </div>
                        <div className="settings-item">
                            <label>Disabled</label>
                            <input type="checkbox" checked={this.state.TPdisabled}
                                onChange={() => this.setState({ TPdisabled: !this.state.TPdisabled })} />
                        </div>
                        <div className="settings-item">
                            <label>Landscape mode</label>
                            <input type="checkbox" checked={(this.state.TPmode as string) === "landscape"}
                                onChange={() => this.setState({ TPmode: (this.state.TPmode as string) === "landscape" ? "portrait" : "landscape" })} />
                        </div>
                    </div>
                    <TimePicker
                        autoSelect={this.state.TPautoSelect}
                        cancelLabel={this.state.TPcancelLabel}
                        defaultTime={this.state.TPdefaultTime}
                        disabled={this.state.TPdisabled}
                        okLabel={this.state.TPokLabel}
                        mode={this.state.TPmode}
                        onChange={(time: string) => { console.log("time picker onChange"); }}
                        onDismiss={() => { console.log("time picker onDismiss"); }}
                        onShow={() => { console.log("time picker onShow"); }}
                    />
                </div>
            </div>
        );
    }

}