import * as React from "react";
import * as ReactDOM from "react-dom";
import { hashHistory, Route, Router, Redirect } from "react-router";

import { PickerDemo } from "./picker";

ReactDOM.render(<PickerDemo />, document.getElementById("app"));