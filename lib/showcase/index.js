"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var picker_1 = require("./picker");
ReactDOM.render(React.createElement(picker_1.PickerDemo, null), document.getElementById("app"));
