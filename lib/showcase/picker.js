"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
// Components
var components_1 = require("../components");
var time_picker_1 = require("../components/time-picker");
var index_1 = require("../components/date-picker/index");
var date_time_utils_1 = require("../components/core/date-time-utils");
var PickerDemo = /** @class */ (function (_super) {
    __extends(PickerDemo, _super);
    function PickerDemo(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            DPautoSelect: false,
            DPcancelLabel: index_1.DEFAULT_LABEL_CANCEL,
            DPdefaultDate: index_1.moment().format(),
            DPdisableYearSelection: false,
            DPdisabled: false,
            DPhideCalendarDate: false,
            DPlocale: index_1.DEFAULT_LOCALE || date_time_utils_1.getUserLanguage(),
            DPmode: "portrait",
            DPokLabel: index_1.DEFAULT_LABEL_OK,
            DPshowNeighboringMonths: false,
            DPmaxDate: index_1.moment().add(index_1.DIFFERENCE_IN_YEARS, "year").format(),
            DPminDate: index_1.moment().subtract(index_1.DIFFERENCE_IN_YEARS, "year").format(),
            TPautoSelect: false,
            TPcancelLabel: index_1.DEFAULT_LABEL_CANCEL,
            TPdefaultTime: index_1.moment().format(),
            TPdisabled: false,
            TPokLabel: index_1.DEFAULT_LABEL_OK,
            TPmode: "portrait"
        };
        return _this;
    }
    PickerDemo.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", null,
            React.createElement("div", null,
                React.createElement("div", null,
                    React.createElement("h3", null, "Settings"),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Auto select"),
                        React.createElement("input", { type: "checkbox", checked: this.state.DPautoSelect, onChange: function () { return _this.setState({ DPautoSelect: !_this.state.DPautoSelect }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Ok label"),
                        React.createElement("input", { type: "text", defaultValue: this.state.DPokLabel, onChange: function (event) {
                                var target = event.target;
                                var value = target.value;
                                _this.setState({
                                    DPokLabel: value
                                });
                            } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Cancel label"),
                        React.createElement("input", { type: "text", defaultValue: this.state.DPcancelLabel, onChange: function (event) {
                                var target = event.target;
                                var value = target.value;
                                _this.setState({
                                    DPcancelLabel: value
                                });
                            } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Default date"),
                        React.createElement("input", { type: "text", defaultValue: this.state.DPdefaultDate, onChange: function (event) {
                                var target = event.target;
                                var value = target.value;
                                _this.setState({
                                    DPdefaultDate: value
                                });
                            } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Disable year selection"),
                        React.createElement("input", { type: "checkbox", checked: this.state.DPdisableYearSelection, onChange: function () { return _this.setState({ DPdisableYearSelection: !_this.state.DPdisableYearSelection }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Hide calendar date"),
                        React.createElement("input", { type: "checkbox", checked: this.state.DPhideCalendarDate, onChange: function () { return _this.setState({ DPhideCalendarDate: !_this.state.DPhideCalendarDate }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Disabled"),
                        React.createElement("input", { type: "checkbox", checked: this.state.DPdisabled, onChange: function () { return _this.setState({ DPdisabled: !_this.state.DPdisabled }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Locale"),
                        React.createElement("select", { value: this.state.DPlocale, onChange: function (event) { return _this.setState({ DPlocale: event.target.value }); } }, index_1.supportedLocales.map(function (lang) {
                            return React.createElement("option", { value: lang.value, key: lang.value }, lang.label);
                        }))),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Landscape mode"),
                        React.createElement("input", { type: "checkbox", checked: this.state.DPmode === "landscape", onChange: function () { return _this.setState({ DPmode: _this.state.DPmode === "landscape" ? "portrait" : "landscape" }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Show neighboring months"),
                        React.createElement("input", { type: "checkbox", checked: this.state.DPshowNeighboringMonths, onChange: function () { return _this.setState({ DPshowNeighboringMonths: !_this.state.DPshowNeighboringMonths }); } }))),
                React.createElement(components_1.DatePicker, { autoSelect: this.state.DPautoSelect, cancelLabel: this.state.DPcancelLabel, defaultDate: this.state.DPdefaultDate, disableYearSelection: this.state.DPdisableYearSelection, disabled: this.state.DPdisabled, hideCalendarDate: this.state.DPhideCalendarDate, locale: this.state.DPlocale, maxDate: this.state.DPmaxDate, minDate: this.state.DPminDate, mode: this.state.DPmode, okLabel: this.state.DPokLabel, onChange: function () { return console.log("change"); }, onDismiss: function () { return console.log("dismiss"); }, onShow: function () { return console.log("show"); }, showNeighboringMonths: this.state.DPshowNeighboringMonths })),
            React.createElement("div", null,
                React.createElement("div", null,
                    React.createElement("h3", null, "Settings"),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Auto select"),
                        React.createElement("input", { type: "checkbox", checked: this.state.TPautoSelect, onChange: function () { return _this.setState({ TPautoSelect: !_this.state.TPautoSelect }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Ok label"),
                        React.createElement("input", { type: "text", defaultValue: this.state.TPokLabel, onChange: function (event) {
                                var target = event.target;
                                var value = target.value;
                                _this.setState({
                                    TPokLabel: value
                                });
                            } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Cancel label"),
                        React.createElement("input", { type: "text", defaultValue: this.state.TPcancelLabel, onChange: function (event) {
                                var target = event.target;
                                var value = target.value;
                                _this.setState({
                                    TPcancelLabel: value
                                });
                            } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Default time"),
                        React.createElement("input", { type: "text", defaultValue: this.state.TPdefaultTime, onChange: function (event) {
                                var target = event.target;
                                var value = target.value;
                                _this.setState({
                                    TPdefaultTime: value
                                });
                            } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Disabled"),
                        React.createElement("input", { type: "checkbox", checked: this.state.TPdisabled, onChange: function () { return _this.setState({ TPdisabled: !_this.state.TPdisabled }); } })),
                    React.createElement("div", { className: "settings-item" },
                        React.createElement("label", null, "Landscape mode"),
                        React.createElement("input", { type: "checkbox", checked: this.state.TPmode === "landscape", onChange: function () { return _this.setState({ TPmode: _this.state.TPmode === "landscape" ? "portrait" : "landscape" }); } }))),
                React.createElement(time_picker_1.TimePicker, { autoSelect: this.state.TPautoSelect, cancelLabel: this.state.TPcancelLabel, defaultTime: this.state.TPdefaultTime, disabled: this.state.TPdisabled, okLabel: this.state.TPokLabel, mode: this.state.TPmode, onChange: function (time) { console.log("time picker onChange"); }, onDismiss: function () { console.log("time picker onDismiss"); }, onShow: function () { console.log("time picker onShow"); } }))));
    };
    return PickerDemo;
}(React.Component));
exports.PickerDemo = PickerDemo;
