export declare function RandomId(size?: number): string;
export declare class AssocArray {
    static size(assoc: Assoc<any>): number;
    static forEach<T>(assoc: Assoc<T>, cb: (item: T, index: string) => void): void;
    static forEach(assoc: Assoc<any>, cb: (item: any, index: string) => void): void;
    static map<T, U>(assoc: Assoc<T>, cb: (item: T, index: string) => U): U[];
    static map<U>(assoc: Assoc<any>, cb: (item: any, index: string) => U): U[];
    static filter<T>(assoc: Assoc<T>, cb: (item: any, index: string) => boolean): Assoc<T>;
    static filter(assoc: Assoc<any>, cb: (item: any, index: string) => boolean): Assoc<any>;
    static sort<T>(assoc: Assoc<T>, cb: (a: {
        key: string;
        value: T;
    }, b: {
        key: string;
        value: T;
    }) => number): Assoc<T>;
}
/**
 * Возвращает первого родителя элемента с заданным именем класса
 * - если родитель не найден, то возвращает null
 *
 * @export
 * @param {HTMLElement} element
 * @param {string} name
 * @returns {HTMLElement}
 */
export declare function getParentByClass(element: HTMLElement, name: string): HTMLElement | null;
/**
 * Экранирование символов в регулярном выражении
 *
 * @param {string} text
 * @returns
 */
export declare function RegExpEscape(text: string): string;
/**
 *
 *
 * @export
 * @param {string} link
 * @param {number} [width=495]
 * @param {number} [height=495]
 */
export declare function windowOpener(link: string, width?: number, height?: number): void;
