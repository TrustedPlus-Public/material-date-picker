"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Объединяет несколько объектов в один
 *
 * @export
 * @param {*} target Объект в который будут добавлены новые свойства.
 * @param {...any[]} sources Объекты, свойства которых будут добавлены в объект `target`
 * @returns Возвращает расширенный объект `target`
 */
function assign(target) {
    var sources = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        sources[_i - 1] = arguments[_i];
    }
    var res = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var obj = arguments[i];
        for (var prop in obj) {
            res[prop] = obj[prop];
        }
    }
    return res;
}
exports.assign = assign;
Object.assign = Object.assign || assign;
