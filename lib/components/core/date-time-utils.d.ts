import * as moment from "moment";
import { Moment } from "moment";
export declare type Affix = 'am' | 'pm';
export declare function getFirstWeekday(): {
    date: moment.Moment;
    sundayWeekday: number;
};
export declare function getUserLanguage(): string;
export declare function getFirstDayOfMonth(date: string): string;
export declare function getLastDayOfMonth(date: string): string;
export declare function isSameDate(date1: string | Moment, date2: string | Moment): boolean;
export declare function isBetweenDate(date: string, before: string, after: string): boolean;
export declare function isAfterDate(date: string, after: string): boolean;
export declare function isBeforeDate(date: string, before: string): boolean;
export declare function getAffix(time: string): Affix;
export declare function rad2deg(rad: number): number;
