"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function RandomId(size) {
    if (size === void 0) { size = 20; }
    return btoa(Math.random().toString().slice(2)).slice(0, size);
}
exports.RandomId = RandomId;
var AssocArray = /** @class */ (function () {
    function AssocArray() {
    }
    AssocArray.size = function (assoc) {
        return Object.keys(assoc).length;
    };
    AssocArray.forEach = function (assoc, cb) {
        for (var key in assoc) {
            cb(assoc[key], key);
        }
    };
    AssocArray.map = function (assoc, cb) {
        var res = [];
        for (var key in assoc) {
            res.push(cb(assoc[key], key));
        }
        return res;
    };
    AssocArray.filter = function (assoc, cb) {
        var res = {};
        for (var key in assoc) {
            if (cb(assoc[key], key))
                res[key] = assoc[key];
        }
        return res;
    };
    AssocArray.sort = function (assoc, cb) {
        var res = {};
        var array = [];
        for (var key in assoc) {
            array.push({ key: key, value: assoc[key] });
        }
        array = array.sort(cb);
        array.forEach(function (item) { return res[item.key] = item.value; });
        return res;
    };
    return AssocArray;
}());
exports.AssocArray = AssocArray;
/**
 * Возвращает первого родителя элемента с заданным именем класса
 * - если родитель не найден, то возвращает null
 *
 * @export
 * @param {HTMLElement} element
 * @param {string} name
 * @returns {HTMLElement}
 */
function getParentByClass(element, name) {
    if (element && element.parentElement) {
        if (element.parentElement.classList.contains(name))
            return element.parentElement;
        else
            return getParentByClass(element.parentElement, name);
    }
    return null;
}
exports.getParentByClass = getParentByClass;
/**
 * Экранирование символов в регулярном выражении
 *
 * @param {string} text
 * @returns
 */
function RegExpEscape(text) {
    return text ? text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") : "";
}
exports.RegExpEscape = RegExpEscape;
/**
 *
 *
 * @export
 * @param {string} link
 * @param {number} [width=495]
 * @param {number} [height=495]
 */
function windowOpener(link, width, height) {
    if (width === void 0) { width = 495; }
    if (height === void 0) { height = 495; }
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
    var w = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var h = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var left = ((w / 2) - (width / 2)) + dualScreenLeft;
    var top = ((h / 2) - (height / 2)) + dualScreenTop - 200;
    var f = window.open(link, "Login", "scrollbars=yes, toolbar=no, location=no, menubar=no, width=" + width + ", height=" + height + ", top=" + top + ", left=" + left);
    // Puts focus on the newWindow
    if (window.focus) {
        f && f.focus();
    }
}
exports.windowOpener = windowOpener;
