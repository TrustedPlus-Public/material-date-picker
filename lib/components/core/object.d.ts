/**
 * Объединяет несколько объектов в один
 *
 * @export
 * @param {*} target Объект в который будут добавлены новые свойства.
 * @param {...any[]} sources Объекты, свойства которых будут добавлены в объект `target`
 * @returns Возвращает расширенный объект `target`
 */
export declare function assign(target: any, ...sources: any[]): any;
