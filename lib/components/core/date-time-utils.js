"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
function getFirstWeekday() {
    var sunday = moment(0).add(3, "day");
    var sundayWeekday = sunday.weekday();
    var date = sunday;
    if (sundayWeekday === 6) {
        date = sunday.add(1, "day");
    }
    return { date: date, sundayWeekday: sundayWeekday };
}
exports.getFirstWeekday = getFirstWeekday;
function getUserLanguage() {
    return navigator.language || navigator.userLanguage;
}
exports.getUserLanguage = getUserLanguage;
function getFirstDayOfMonth(date) {
    return moment(date).startOf("month").format();
}
exports.getFirstDayOfMonth = getFirstDayOfMonth;
function getLastDayOfMonth(date) {
    return moment(date).endOf("month").format();
}
exports.getLastDayOfMonth = getLastDayOfMonth;
function isSameDate(date1, date2) {
    return moment(date1).startOf("day").isSame(moment(date2).startOf("day"), "date");
}
exports.isSameDate = isSameDate;
function isBetweenDate(date, before, after) {
    return moment(date).isBetween(before, after);
}
exports.isBetweenDate = isBetweenDate;
function isAfterDate(date, after) {
    return moment(date).isAfter(after);
}
exports.isAfterDate = isAfterDate;
function isBeforeDate(date, before) {
    return moment(date).isBefore(before);
}
exports.isBeforeDate = isBeforeDate;
function getAffix(time) {
    return moment(time).hours() >= 12 ? "pm" : "am";
}
exports.getAffix = getAffix;
function rad2deg(rad) {
    return rad * 57.29577951308232;
}
exports.rad2deg = rad2deg;
