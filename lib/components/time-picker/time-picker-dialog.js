"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_dom_1 = require("react-dom");
var classnames = require("classnames");
var clock_1 = require("./clock");
var TimePickerDialog = /** @class */ (function (_super) {
    __extends(TimePickerDialog, _super);
    function TimePickerDialog(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            isOpened: false
        };
        _this.open = _this.open.bind(_this);
        _this.close = _this.close.bind(_this);
        _this.handleAccept = _this.handleAccept.bind(_this);
        _this.handleDismiss = _this.handleDismiss.bind(_this);
        _this.handleChangeMinutes = _this.handleChangeMinutes.bind(_this);
        _this.handleWrapperClick = _this.handleWrapperClick.bind(_this);
        _this.handleWindowKeyUp = _this.handleWindowKeyUp.bind(_this);
        return _this;
    }
    TimePickerDialog.prototype.open = function () {
        var _this = this;
        this.setState({
            isOpened: true
        }, function () {
            react_dom_1.findDOMNode(_this.refs["root"]).focus();
        });
    };
    TimePickerDialog.prototype.close = function () {
        this.props.onDismiss && this.props.onDismiss();
        this.setState({
            isOpened: false
        });
    };
    TimePickerDialog.prototype.handleAccept = function () {
        var time = this.refs["clock"].getTime();
        this.props.onAccept && this.props.onAccept(time);
        this.setState({
            isOpened: false
        });
    };
    TimePickerDialog.prototype.handleWindowKeyUp = function (event) {
        var keyCode = event.keyCode;
        switch (keyCode) {
            case 13:
                this.handleAccept();
                break;
            case 27:
                this.handleDismiss();
                break;
            default:
                break;
        }
        event.preventDefault();
        event.stopPropagation();
        return false;
    };
    TimePickerDialog.prototype.handleDismiss = function () {
        this.refs["clock"].setMode("h");
        this.props.onDismiss && this.props.onDismiss();
        this.setState({
            isOpened: false
        });
    };
    TimePickerDialog.prototype.handleWrapperClick = function (event) {
        var target = event.target;
        if (target.classList.contains("time-picker-dialog-wrapper")) {
            this.handleDismiss();
        }
    };
    TimePickerDialog.prototype.handleChangeMinutes = function (time) {
        if (this.props.autoSelect) {
            this.handleAccept();
        }
    };
    TimePickerDialog.prototype.render = function () {
        var _a = this.props, cancelLabel = _a.cancelLabel, okLabel = _a.okLabel, autoSelect = _a.autoSelect, initialTime = _a.initialTime, mode = _a.mode;
        return (React.createElement("div", { className: classnames("time-picker-dialog-root", { opened: this.state.isOpened }) },
            React.createElement("div", { className: "time-picker-dialog-wrapper", ref: "root", tabIndex: 2, onClick: this.handleWrapperClick },
                React.createElement("div", { className: "time-picker-dialog-content" },
                    React.createElement(clock_1.Clock, { viewMode: mode, ref: "clock", autoSelect: autoSelect, initialTime: initialTime, onAcceptClick: this.handleAccept, onDismissClick: this.handleDismiss, cancelLabel: cancelLabel, okLabel: okLabel, onChangeMinutes: this.handleChangeMinutes })))));
    };
    return TimePickerDialog;
}(React.Component));
exports.TimePickerDialog = TimePickerDialog;
