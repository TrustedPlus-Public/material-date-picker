import * as React from "react";
import { SlideDirection } from "../slideIn-transition-group/slideIn-transition-group";
interface ITimeSelectorProps {
    value: number;
    minValue: number;
    maxValue: number;
    onChange: (newValue: number) => void;
    tabIndex: number;
}
interface ITimeSelectorState {
    value: number;
    direction: SlideDirection;
}
export declare class TimeSelector extends React.Component<ITimeSelectorProps, ITimeSelectorState> {
    applyingValue: number;
    constructor(props: ITimeSelectorProps);
    componentWillReceiveProps(nextProps: ITimeSelectorProps): void;
    changeValue(additionalValue: number): boolean;
    shouldComponentUpdate(nextProps: ITimeSelectorProps): boolean;
    handleChangeValue(target: any): void;
    handleKeyUp(event: any): boolean;
    render(): JSX.Element;
}
export {};
