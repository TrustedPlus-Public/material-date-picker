import * as React from "react";
import { Affix, ClockMode } from "./index";
interface ITimeDisplayProps {
    initialTime: string;
    onSelectAffix: (affix: Affix) => void;
    onSelectHour: () => void;
    onSelectMin: () => void;
    mode: ClockMode;
    affix: Affix;
}
export declare class TimeDisplay extends React.Component<ITimeDisplayProps, {}> {
    render(): JSX.Element;
}
export {};
