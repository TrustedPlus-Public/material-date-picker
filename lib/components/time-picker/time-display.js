"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var index_1 = require("./index");
var classnames = require("classnames");
var TimeDisplay = /** @class */ (function (_super) {
    __extends(TimeDisplay, _super);
    function TimeDisplay() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TimeDisplay.prototype.render = function () {
        var _a = this.props, affix = _a.affix, initialTime = _a.initialTime, onSelectAffix = _a.onSelectAffix, onSelectHour = _a.onSelectHour, onSelectMin = _a.onSelectMin, mode = _a.mode;
        var time = index_1.moment(initialTime);
        var hours = time.hours();
        var hoursLabel = (hours === 12 || hours === 0) ? affix === "am" ? 0 : 12 : hours % 12;
        var minutes = time.format("mm");
        return (React.createElement("div", { className: "display-time--root" },
            React.createElement("div", { className: "display-time--content" },
                React.createElement("div", { className: classnames("display-hours", { "clock-display-time--item-active": mode === "h" }), onClick: onSelectHour }, hoursLabel),
                React.createElement("div", { className: "time-separator" }, ":"),
                React.createElement("div", { className: classnames("display-minutes", { "clock-display-time--item-active": mode === "m" }), onClick: onSelectMin }, minutes)),
            React.createElement("div", { className: "select-affix" },
                React.createElement("div", { onClick: function () { return onSelectAffix("am"); }, className: classnames("affix-value affix-am", { "affix-current": affix === "am" }) }, "am"),
                React.createElement("div", { onClick: function () { return onSelectAffix("pm"); }, className: classnames("affix-value affix-pm", { "affix-current": affix === "pm" }) }, "pm"))));
    };
    return TimeDisplay;
}(React.Component));
exports.TimeDisplay = TimeDisplay;
