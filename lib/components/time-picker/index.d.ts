import * as moment from "moment";
import { Moment } from "moment";
import { DEFAULT_LABEL_CANCEL, DEFAULT_LABEL_OK } from "../date-picker/index";
export { Moment, moment, DEFAULT_LABEL_OK, DEFAULT_LABEL_CANCEL };
export declare type ClockMode = "h" | "m";
export declare type Affix = "am" | "pm";
export * from "./time-picker";
