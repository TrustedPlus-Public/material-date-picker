"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
exports.moment = moment;
var index_1 = require("../date-picker/index");
exports.DEFAULT_LABEL_CANCEL = index_1.DEFAULT_LABEL_CANCEL;
exports.DEFAULT_LABEL_OK = index_1.DEFAULT_LABEL_OK;
__export(require("./time-picker"));
