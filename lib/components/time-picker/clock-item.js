"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
function isMousePressed(event) {
    if (typeof event.buttons === "undefined") {
        return event.nativeEvent.which;
    }
    return event.buttons;
}
function removeClassName(element, className) {
    element && element.classList.remove(className);
}
function addClassName(element, className) {
    element && element.classList.add(className);
}
var ClockItems = /** @class */ (function (_super) {
    __extends(ClockItems, _super);
    function ClockItems() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClockItems.prototype.shouldComponentUpdate = function (nextProps) {
        var currentValueChanged = this.props.currentValue !== nextProps.currentValue;
        var typeChanged = this.props.type !== nextProps.type;
        var initialValueChanged = this.props.initialValue !== nextProps.initialValue;
        var affixChanged = (nextProps.type === "hours" && (nextProps.affix !== this.props.affix));
        return initialValueChanged || typeChanged || currentValueChanged || affixChanged;
    };
    ClockItems.prototype.getItems = function (count) {
        var _a = this.props, initialValue = _a.initialValue, currentValue = _a.currentValue, type = _a.type, affix = _a.affix;
        var current = currentValue;
        if (type === "hours" && affix === "pm") {
            current = currentValue % 12;
        }
        var clockItems = [];
        for (var i = 0; i < count; i++) {
            var clockItem = {
                label: "" + i,
                isCurrent: current === i,
                isSelected: initialValue === i,
                value: i
            };
            if (type === "hours" && i === 0 && affix === "pm") {
                clockItem.label = "" + 12;
            }
            clockItems.push(clockItem);
        }
        return clockItems;
    };
    ClockItems.prototype.render = function () {
        var _a = this.props, type = _a.type, onChange = _a.onChange;
        var count = this.props.type === "hours" ? 12 : 60;
        var items = this.getItems(count);
        return (React.createElement("div", { className: "display-" + type }, items.map(function (item) {
            var label = item.label, isCurrent = item.isCurrent, isSelected = item.isSelected, value = item.value;
            var itemKey = type.slice(0, -1) + "-" + label;
            var itemClassName = classnames(type, itemKey, {
                "clock-item--current": isCurrent,
                "clock-item--selected": isSelected
            });
            return (React.createElement("div", { className: itemClassName, key: itemKey, ref: itemKey, "data-value": value, onMouseOver: function (event) { return isMousePressed(event) && onChange(value, false); }, onMouseUp: function (event) { return onChange(value); } }, label));
        })));
    };
    return ClockItems;
}(React.Component));
exports.ClockItems = ClockItems;
