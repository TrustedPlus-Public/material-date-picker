"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var index_1 = require("../input/index");
var object_1 = require("../core/object");
var index_2 = require("./index");
var time_picker_dialog_1 = require("./time-picker-dialog");
var TimePicker = /** @class */ (function (_super) {
    __extends(TimePicker, _super);
    function TimePicker(props) {
        var _this = _super.call(this, props) || this;
        _this.assignedProps = object_1.assign(TimePicker.defaultProps, props);
        _this.state = {
            time: _this.assignedProps.defaultTime
        };
        _this.handleAccept = _this.handleAccept.bind(_this);
        return _this;
    }
    TimePicker.prototype.componentWillReceiveProps = function (nextProps) {
        var newState = {};
        if (nextProps.defaultTime !== undefined && this.props.defaultTime !== nextProps.defaultTime) {
            newState.time = nextProps.defaultTime;
        }
        this.setState(newState);
    };
    TimePicker.prototype.componentWillMount = function () {
        this.setState({
            time: this.assignedProps.defaultTime
        });
    };
    TimePicker.prototype.handleAccept = function (time) {
        this.setState({
            time: time
        });
        this.props.onChange && this.props.onChange(time);
    };
    TimePicker.prototype.render = function () {
        var _this = this;
        this.assignedProps = object_1.assign(TimePicker.defaultProps, this.props);
        var _a = this.assignedProps, autoSelect = _a.autoSelect, cancelLabel = _a.cancelLabel, className = _a.className, disabled = _a.disabled, okLabel = _a.okLabel, onDismiss = _a.onDismiss, onShow = _a.onShow, mode = _a.mode, ico = _a.ico;
        return (React.createElement("div", { className: "time-picker-root " + className, tabIndex: 2 },
            React.createElement(index_1.InputField, { ico: ico, className: "time-picker--input-field", disabled: disabled, value: index_2.moment(this.state.time).format("LT"), onClick: function () { return _this.refs["pickerDialog"].open(); }, onChange: function () { return void 0; } }),
            React.createElement(time_picker_dialog_1.TimePickerDialog, { initialTime: this.state.time, autoSelect: autoSelect, cancelLabel: cancelLabel, okLabel: okLabel, onAccept: this.handleAccept, onDismiss: onDismiss, onShow: onShow, mode: mode, ref: "pickerDialog" })));
    };
    TimePicker.defaultProps = {
        autoSelect: false,
        cancelLabel: index_2.DEFAULT_LABEL_CANCEL,
        defaultTime: index_2.moment().format(),
        disabled: false,
        okLabel: index_2.DEFAULT_LABEL_OK,
        onChange: function (date) { return void 0; },
        onDismiss: function () { return void 0; },
        onShow: function () { return void 0; },
        mode: "portrait"
    };
    return TimePicker;
}(React.Component));
exports.TimePicker = TimePicker;
