"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var index_1 = require("../button/index");
var slideIn_transition_group_1 = require("../slideIn-transition-group/slideIn-transition-group");
var TimeSelector = /** @class */ (function (_super) {
    __extends(TimeSelector, _super);
    function TimeSelector(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            value: props.value,
            direction: "up"
        };
        _this.changeValue = _this.changeValue.bind(_this);
        _this.handleKeyUp = _this.handleKeyUp.bind(_this);
        _this.handleChangeValue = _this.handleChangeValue.bind(_this);
        return _this;
    }
    TimeSelector.prototype.componentWillReceiveProps = function (nextProps) {
        this.applyingValue = -1;
        this.setState({
            value: nextProps.value,
            direction: this.props.value < nextProps.value ? "up" : "down"
        });
    };
    TimeSelector.prototype.changeValue = function (additionalValue) {
        var newValue = this.state.value + additionalValue;
        var _a = this.props, minValue = _a.minValue, maxValue = _a.maxValue;
        if (maxValue > -1 && minValue > -1) {
            if (newValue >= minValue && newValue <= maxValue) {
                this.props.onChange && this.props.onChange(newValue);
            }
        }
        else {
            this.props.onChange && this.props.onChange(newValue);
        }
        return false;
    };
    TimeSelector.prototype.shouldComponentUpdate = function (nextProps) {
        return this.state.value !== nextProps.value;
    };
    TimeSelector.prototype.handleChangeValue = function (target) {
        var _this = this;
        var value = target.textContent * 1;
        var newValue = value;
        if (value > this.props.maxValue) {
            newValue = this.props.maxValue;
        }
        else if (value < this.props.minValue) {
            newValue = this.props.minValue;
        }
        setTimeout(function () {
            var value = target.textContent * 1;
            if (_this.applyingValue === newValue) {
                _this.props.onChange(newValue);
            }
            else {
                _this.applyingValue = newValue;
                _this.handleChangeValue(target);
            }
        }, 250);
    };
    TimeSelector.prototype.handleKeyUp = function (event) {
        return false;
        // const {keyCode} = event;
        // if ([46, 8, 9, 27, 13, 110, 190].indexOf(keyCode) !== -1 ||
        //     (keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) ||
        //     (keyCode >= 35 && keyCode <= 40)) {
        //     return;
        // }
        // if ((event.shiftKey || (keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
        //     event.preventDefault();
        // } else {
        //     this.handleChangeValue(event.target);
        // }
    };
    TimeSelector.prototype.render = function () {
        var _this = this;
        var text = this.state.value >= 10 ? "" + this.state.value : "0" + this.state.value;
        return (React.createElement("div", { className: "time-selector--root" },
            React.createElement(index_1.FlatButton, { onClick: function () {
                    _this.changeValue(1);
                } },
                React.createElement("i", { className: "material-icons" }, "expand_less")),
            React.createElement(slideIn_transition_group_1.SlideInTransitionGroup, { direction: this.state.direction },
                React.createElement("div", { className: "time-selector--value", tabIndex: this.props.tabIndex || 0, onKeyUpCapture: this.handleKeyUp }, text)),
            React.createElement(index_1.FlatButton, { onClick: function () {
                    _this.changeValue(-1);
                } },
                React.createElement("i", { className: "material-icons" }, "expand_more"))));
    };
    return TimeSelector;
}(React.Component));
exports.TimeSelector = TimeSelector;
