import * as React from "react";
interface ITimePickerState {
    time: string;
}
export interface ITimePickerProps {
    autoSelect?: boolean;
    cancelLabel?: string;
    className?: string;
    defaultTime?: string;
    disabled?: boolean;
    okLabel?: string;
    onChange?: (date: string) => void;
    onDismiss?: () => void;
    onShow?: () => void;
    mode?: "portrait" | "landscape";
    ico?: string;
}
export declare class TimePicker extends React.Component<ITimePickerProps, ITimePickerState> {
    assignedProps: ITimePickerProps;
    static defaultProps: ITimePickerProps;
    constructor(props: ITimePickerProps);
    componentWillReceiveProps(nextProps: ITimePickerProps): void;
    componentWillMount(): void;
    private handleAccept;
    render(): JSX.Element;
}
export {};
