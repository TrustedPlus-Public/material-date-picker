import * as React from "react";
interface ITimePickerDialogProps {
    initialTime: string;
    autoSelect: boolean;
    cancelLabel: string;
    okLabel: string;
    onAccept: (time: string) => void;
    onDismiss: () => void;
    onShow: () => void;
    mode: "portrait" | "landscape";
}
interface ITimePickerDialogState {
    isOpened: boolean;
}
export declare class TimePickerDialog extends React.Component<ITimePickerDialogProps, ITimePickerDialogState> {
    constructor(props: ITimePickerDialogProps);
    open(): void;
    close(): void;
    private handleAccept;
    private handleWindowKeyUp;
    private handleDismiss;
    private handleWrapperClick;
    private handleChangeMinutes;
    render(): JSX.Element;
}
export {};
