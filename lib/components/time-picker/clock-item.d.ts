import * as React from "react";
import { Affix } from "./index";
interface IClockItemsProps {
    type: "hours" | "minutes";
    onChange: (time: number, mustCallTimeChange?: boolean) => void;
    changeArrowAngle: (value: number) => void;
    initialValue: number;
    currentValue: number;
    affix?: Affix;
}
interface IClockItemsState {
}
export declare class ClockItems extends React.Component<IClockItemsProps, IClockItemsState> {
    shouldComponentUpdate(nextProps: IClockItemsProps): boolean;
    private getItems;
    render(): JSX.Element;
}
export {};
