import * as React from "react";
import { ClockMode } from "./index";
interface IClockProps {
    viewMode: "portrait" | "landscape";
    initialTime: string;
    onChangeMinutes: (time: string) => void;
    autoSelect?: boolean;
    onChangeHours?: (time: string) => void;
    onAcceptClick?: () => void;
    onDismissClick?: () => void;
    cancelLabel?: string;
    okLabel?: string;
}
interface IClockState {
    mode: ClockMode;
    selectedTime: string;
}
export declare class Clock extends React.Component<IClockProps, IClockState> {
    private center;
    private basePoint;
    private arrowAngle;
    private assignedProps;
    static defaultProps: {
        viewMode: string;
        initialTime: string;
        onChangeHours: (time: string) => undefined;
        onChangeMinutes: (time: string) => undefined;
        autoSelect: boolean;
        onAcceptClick: () => undefined;
        onDismissClick: () => undefined;
        cancelLabel: string;
        okLabel: string;
    };
    constructor(props: IClockProps);
    shouldComponentUpdate(nextProps: IClockProps, nextState: IClockState): boolean;
    componentWillMount(): void;
    componentDidMount(): void;
    componentWillReceiveProps(nextProps: IClockProps): void;
    setMode(mode: ClockMode): void;
    getTime(): string;
    private handleSelectAffix;
    private handleChangeHours;
    private handleChangeMinutes;
    private getAngleByTimeValue;
    private handleChangeMode;
    private handleChangeAffix;
    private handleChangeArrowAngle;
    private getTimeByEvent;
    private handleMouseMove;
    private handleMouseUp;
    private handleAcceptClick;
    render(): JSX.Element;
}
export {};
