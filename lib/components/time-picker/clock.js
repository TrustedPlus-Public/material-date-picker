"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var index_1 = require("./index");
var clock_item_1 = require("./clock-item");
var time_display_1 = require("./time-display");
var object_1 = require("../core/object");
var date_time_utils_1 = require("../core/date-time-utils");
var index_2 = require("../button/index");
var classnames = require("classnames");
var MUST_AUTOCHANGE_MODE = false;
function isMousePressed(event) {
    if (typeof event.buttons === "undefined") {
        return event.nativeEvent.which;
    }
    return event.buttons;
}
var Clock = /** @class */ (function (_super) {
    __extends(Clock, _super);
    function Clock(props) {
        var _this = _super.call(this, props) || this;
        _this.assignedProps = object_1.assign(Clock.defaultProps, props);
        var initialTime = props.initialTime || index_1.moment().toISOString();
        _this.state = {
            mode: "h",
            selectedTime: initialTime
        };
        _this.handleChangeHours = _this.handleChangeHours.bind(_this);
        _this.handleChangeMinutes = _this.handleChangeMinutes.bind(_this);
        _this.handleSelectAffix = _this.handleSelectAffix.bind(_this);
        _this.handleChangeAffix = _this.handleChangeAffix.bind(_this);
        _this.handleChangeArrowAngle = _this.handleChangeArrowAngle.bind(_this);
        _this.setMode = _this.setMode.bind(_this);
        _this.getTime = _this.getTime.bind(_this);
        _this.getTimeByEvent = _this.getTimeByEvent.bind(_this);
        _this.handleMouseUp = _this.handleMouseUp.bind(_this);
        _this.handleMouseMove = _this.handleMouseMove.bind(_this);
        _this.handleAcceptClick = _this.handleAcceptClick.bind(_this);
        return _this;
    }
    Clock.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var selectedTime = this.state.selectedTime;
        var nextSelectedTime = nextState.selectedTime;
        var affixChanged = date_time_utils_1.getAffix(selectedTime) !== date_time_utils_1.getAffix(nextSelectedTime);
        var selectedTimeChanged = nextSelectedTime !== selectedTime;
        var modeChanged = this.state.mode !== nextState.mode;
        var initialTimeChanged = this.props.initialTime !== nextProps.initialTime;
        var viewModeChanged = this.props.viewMode !== nextProps.viewMode;
        var autoSelectChanged = this.props.autoSelect !== nextProps.autoSelect;
        return selectedTimeChanged || affixChanged || modeChanged || initialTimeChanged || viewModeChanged || autoSelectChanged;
    };
    Clock.prototype.componentWillMount = function () {
        var initialTime = this.assignedProps.initialTime;
        this.setState({
            selectedTime: initialTime,
            mode: "h"
        });
    };
    Clock.prototype.componentDidMount = function () {
        var clockElement = ReactDOM.findDOMNode(this.refs["mask"]);
        this.center = {
            x: clockElement.offsetWidth / 2,
            y: clockElement.offsetHeight / 2
        };
        this.basePoint = {
            x: this.center.x,
            y: 0
        };
        this.handleChangeMode(this.state.mode);
    };
    Clock.prototype.componentWillReceiveProps = function (nextProps) {
        var _this = this;
        var initialTime = nextProps.initialTime;
        var mode = this.state.mode;
        var angle = this.getAngleByTimeValue(initialTime, mode);
        this.setState({
            selectedTime: initialTime,
            mode: "h"
        }, function () { return _this.handleChangeArrowAngle(angle); });
    };
    Clock.prototype.setMode = function (mode) {
        this.setState({
            mode: mode
        });
    };
    Clock.prototype.getTime = function () {
        return this.state.selectedTime;
    };
    Clock.prototype.handleSelectAffix = function (affix) {
        if (affix === date_time_utils_1.getAffix(this.state.selectedTime)) {
            return;
        }
        var hours = index_1.moment(this.state.selectedTime).hours();
        var newHours = hours + 12;
        if (affix === "am") {
            newHours = hours % 13;
        }
        this.handleChangeHours(index_1.moment().hours(newHours).hours(), affix);
    };
    ;
    Clock.prototype.handleChangeHours = function (time, finished) {
        var _this = this;
        var selectedTime = this.state.selectedTime;
        var affix;
        if (typeof finished === "string") {
            affix = finished;
            finished = undefined;
        }
        if (!affix) {
            affix = date_time_utils_1.getAffix(selectedTime);
        }
        if (affix === "pm") {
            time = time + 12;
        }
        else {
            time = time % 12;
        }
        var newTime = index_1.moment(selectedTime).hours(time).format();
        if (newTime !== selectedTime) {
            this.setState({
                selectedTime: newTime
            });
        }
        if (finished) {
            setTimeout(function () {
                MUST_AUTOCHANGE_MODE && _this.setState({
                    mode: "m"
                });
                _this.props.onChangeHours && _this.props.onChangeHours(newTime);
            }, 100);
        }
    };
    Clock.prototype.handleChangeMinutes = function (time, finished) {
        var _this = this;
        if (finished === void 0) { finished = true; }
        var selectedTime = this.state.selectedTime;
        var hours = index_1.moment(selectedTime).hours();
        var newTime = index_1.moment(selectedTime).hours(hours).minutes(time).format();
        this.setState({
            selectedTime: newTime
        });
        if (this.props.onChangeMinutes && finished) {
            setTimeout(function () {
                _this.props.onChangeMinutes(newTime);
            }, 0);
        }
    };
    Clock.prototype.getAngleByTimeValue = function (selectedTime, mode) {
        var time = index_1.moment(selectedTime);
        var value;
        var step;
        if (mode === "h") {
            value = time.hours() % 12;
            step = 360 / 12;
        }
        else {
            value = time.minutes();
            step = 360 / 60;
        }
        return value * step;
    };
    Clock.prototype.handleChangeMode = function (mode) {
        var selectedTime = this.state.selectedTime;
        var angle = this.getAngleByTimeValue(selectedTime, mode);
        this.setMode(mode);
        this.handleChangeArrowAngle(angle);
    };
    Clock.prototype.handleChangeAffix = function (affix) {
        var selectedTime = this.state.selectedTime;
        if (affix !== date_time_utils_1.getAffix(selectedTime)) {
            var hours = index_1.moment(selectedTime).hours();
            if (affix === "am") {
                hours = hours % 12;
            }
            this.handleChangeHours(hours, affix);
        }
    };
    Clock.prototype.handleChangeArrowAngle = function (angle) {
        this.arrowAngle = angle;
        this.refs["arrow"].style.transform = "rotate(" + angle + "deg)";
    };
    Clock.prototype.getTimeByEvent = function (event) {
        var mode = this.state.mode;
        var items = 12;
        if (mode === "m") {
            items = 60;
        }
        var step = 360 / items;
        var nativeEvent = event.nativeEvent;
        var ex = nativeEvent.offsetX;
        var ey = nativeEvent.offsetY;
        var center = {
            x: 150,
            y: 150
        };
        var x = ex - center.x;
        var y = (ey - center.y) * -1;
        var isXneg = false;
        var isYneg = false;
        if (x < 0) {
            isXneg = true;
            x = Math.abs(x);
        }
        if (y < 0) {
            isYneg = true;
            y = Math.abs(y);
        }
        var atan = Math.atan2(x, y);
        var deg = date_time_utils_1.rad2deg(atan);
        deg = Math.round(deg / step) * step;
        deg %= 360;
        if (isYneg && isXneg) {
            deg += 180;
        }
        else if (isYneg) {
            deg = 180 - deg;
        }
        else if (isXneg) {
            deg = 360 - deg;
        }
        this.arrowAngle !== deg && this.handleChangeArrowAngle(deg);
        return (deg / step) % items;
    };
    Clock.prototype.handleMouseMove = function (event) {
        event.preventDefault();
        if (isMousePressed(event)) {
            var value = this.getTimeByEvent(event);
            var selectedTime = index_1.moment(this.state.selectedTime);
            if (this.state.mode === "h") {
                selectedTime.hours() !== value && this.handleChangeHours(value, false);
            }
            else {
                selectedTime.minutes() !== value && this.handleChangeMinutes(value, false);
            }
        }
        return false;
    };
    Clock.prototype.handleMouseUp = function (event) {
        event.preventDefault();
        var value = this.getTimeByEvent(event);
        if (this.state.mode === "h") {
            this.handleChangeHours(value, true);
            MUST_AUTOCHANGE_MODE && this.handleChangeMode("m");
        }
        else {
            this.handleChangeMinutes(value, true);
        }
        return;
    };
    Clock.prototype.handleAcceptClick = function () {
        // if (this.state.mode === "m") {
        this.props.onAcceptClick && this.props.onAcceptClick();
        // } else {
        //     this.handleChangeMode("m");
        // }
    };
    Clock.prototype.render = function () {
        var _this = this;
        var clock;
        var _a = this.state, selectedTime = _a.selectedTime, mode = _a.mode;
        var momentSelected = index_1.moment(this.state.selectedTime);
        var _b = this.props, viewMode = _b.viewMode, autoSelect = _b.autoSelect, cancelLabel = _b.cancelLabel, okLabel = _b.okLabel, onDismissClick = _b.onDismissClick;
        var affix = date_time_utils_1.getAffix(selectedTime);
        if (this.state.mode === "h") {
            clock = React.createElement(clock_item_1.ClockItems, { key: "hours", ref: "hours", type: "hours", onChange: this.handleChangeHours, affix: affix, initialValue: momentSelected.hours() % 12, currentValue: index_1.moment().hours(), changeArrowAngle: this.handleChangeArrowAngle });
        }
        else {
            clock = React.createElement(clock_item_1.ClockItems, { key: "minutes", ref: "minutes", type: "minutes", onChange: this.handleChangeMinutes, initialValue: momentSelected.minutes(), currentValue: index_1.moment().minutes(), changeArrowAngle: this.handleChangeArrowAngle });
        }
        return (React.createElement("div", { className: classnames("clock-root", "clock-mode--" + viewMode) },
            React.createElement(time_display_1.TimeDisplay, { affix: affix, mode: mode, initialTime: selectedTime, onSelectAffix: this.handleChangeAffix, onSelectHour: function () { return _this.handleChangeMode("h"); }, onSelectMin: function () { return _this.handleChangeMode("m"); } }),
            React.createElement("div", { className: "clock--face" },
                React.createElement("div", { className: "display-clock-item" },
                    React.createElement("div", { className: "arrow", ref: "arrow" }),
                    React.createElement("div", { className: "digts" }, clock),
                    React.createElement("div", { className: "clock-mask", ref: "mask", onMouseMove: this.handleMouseMove, onMouseUp: this.handleMouseUp })),
                !autoSelect &&
                    React.createElement("div", { className: "time-picker-actions" },
                        React.createElement(index_2.FlatButton, { color: "date-blue", className: "calendar-action-button--cancel", onClick: onDismissClick }, cancelLabel),
                        React.createElement(index_2.FlatButton, { color: "date-blue", className: "calendar-action-button--submit", onClick: this.handleAcceptClick }, okLabel)))));
    };
    Clock.defaultProps = {
        viewMode: "portrait",
        initialTime: index_1.moment().format(),
        onChangeHours: function (time) { return void 0; },
        onChangeMinutes: function (time) { return void 0; },
        autoSelect: false,
        onAcceptClick: function () { return void 0; },
        onDismissClick: function () { return void 0; },
        cancelLabel: index_1.DEFAULT_LABEL_CANCEL,
        okLabel: index_1.DEFAULT_LABEL_OK
    };
    return Clock;
}(React.Component));
exports.Clock = Clock;
