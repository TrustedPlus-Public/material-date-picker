import * as React from "react";
export interface ICalendarActionButtonsProps {
    cancelLabel: string;
    submitLabel: string;
    cancelTitle?: string;
    submitTitle?: string;
    onCancelClick: (event: any) => void;
    onSubmitClick: (event: any) => void;
}
interface ICalendarActionButtonsState {
}
export declare class CalendarActionButtons extends React.Component<ICalendarActionButtonsProps, ICalendarActionButtonsState> {
    render(): JSX.Element;
}
export {};
