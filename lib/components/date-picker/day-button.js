"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var DayButton = /** @class */ (function (_super) {
    __extends(DayButton, _super);
    function DayButton(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            isSelected: props.isSelected || false
        };
        return _this;
    }
    DayButton.prototype.select = function () {
        this.setState({
            isSelected: true
        });
    };
    DayButton.prototype.unselect = function () {
        this.setState({
            isSelected: false
        });
    };
    DayButton.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", { className: "calendar-week--day " + this.props.className + " " + classnames({ "day-selected": this.state.isSelected, "day-disabled": this.props.disabled }) }, this.props.children
            ?
                React.createElement("div", { className: "day-button", onClick: function (event) {
                        !_this.props.disabled && _this.props.onClick(event);
                    } }, this.props.children)
            : ""));
    };
    return DayButton;
}(React.Component));
exports.DayButton = DayButton;
