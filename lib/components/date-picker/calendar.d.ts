import * as React from "react";
import { SlideDirection } from "../slideIn-transition-group";
import { Mode } from "./index";
interface ICalendarProps {
    autoSelect?: boolean;
    cancelLabel?: string;
    disableYearSelection?: boolean;
    hideCalendarDate?: boolean;
    initialDate?: string;
    locale?: string;
    showNeighboringMonths?: boolean;
    onDayClick?: () => void;
    onAccept?: (event: any) => void;
    onDismiss?: (event: any) => void;
    maxDate?: string;
    minDate?: string;
    okLabel?: string;
    mode?: Mode;
}
interface ICalendarState {
    displayDate: string;
    selectedDate: string;
    displayMonthDay: boolean;
    transitionDirection: SlideDirection;
}
export declare class Calendar extends React.Component<ICalendarProps, ICalendarState> {
    static defaultProps: ICalendarProps;
    protected assignedProps: ICalendarProps;
    constructor(props: ICalendarProps);
    componentWillMount(): void;
    componentWillReceiveProps(nextProps: ICalendarProps): void;
    private setDisplayDate;
    private setSelectedDate;
    getSelectedDate(): string;
    private handleMonthChange;
    private getToolbarInteractions;
    private changeDisplayDate;
    private handleKeyDown;
    private handleSelectDate;
    private handleYearSelect;
    private handleDateDisplayYearClick;
    private handleDateDisplayMonthClick;
    render(): JSX.Element;
}
export {};
