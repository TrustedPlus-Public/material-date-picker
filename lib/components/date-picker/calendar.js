"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var object_1 = require("../core/object");
var date_time_utils_1 = require("../core/date-time-utils");
var date_display_1 = require("./date-display");
var calendar_toolbar_1 = require("./calendar-toolbar");
var calendar_days_1 = require("./calendar-days");
var calendar_month_1 = require("./calendar-month");
var calendar_year_1 = require("./calendar-year");
var calendar_action_buttons_1 = require("./calendar-action-buttons");
var index_1 = require("./index");
var Calendar = /** @class */ (function (_super) {
    __extends(Calendar, _super);
    function Calendar(props) {
        var _this = _super.call(this, props) || this;
        _this.assignedProps = object_1.assign(Calendar.defaultProps, props);
        var locale = _this.assignedProps.locale;
        index_1.moment.locale(locale);
        var initialDate = props.initialDate || "";
        var selectedDate = index_1.moment(initialDate).format();
        _this.state = {
            displayDate: selectedDate,
            selectedDate: selectedDate,
            displayMonthDay: true,
            transitionDirection: "left"
        };
        _this.handleMonthChange = _this.handleMonthChange.bind(_this);
        _this.handleSelectDate = _this.handleSelectDate.bind(_this);
        _this.handleDateDisplayMonthClick = _this.handleDateDisplayMonthClick.bind(_this);
        _this.handleDateDisplayYearClick = _this.handleDateDisplayYearClick.bind(_this);
        _this.handleYearSelect = _this.handleYearSelect.bind(_this);
        _this.getSelectedDate = _this.getSelectedDate.bind(_this);
        return _this;
    }
    Calendar.prototype.componentWillMount = function () {
        var initialDate = this.assignedProps.initialDate;
        this.setState({
            displayDate: date_time_utils_1.getFirstDayOfMonth(initialDate),
            selectedDate: initialDate
        });
    };
    Calendar.prototype.componentWillReceiveProps = function (nextProps) {
        var mustUpdateState = false;
        var newState = {};
        if (this.assignedProps.locale !== nextProps.locale || index_1.moment.locale() !== nextProps.locale) {
            index_1.moment.locale(nextProps.locale);
            mustUpdateState = true;
        }
        var date = nextProps.initialDate || index_1.moment().format();
        var firstDay = date_time_utils_1.getFirstDayOfMonth(date);
        if (this.state.selectedDate !== nextProps.initialDate || this.props.initialDate !== nextProps.initialDate || this.state.displayDate !== firstDay) {
            mustUpdateState = true;
            newState = {
                displayDate: firstDay,
                selectedDate: date
            };
        }
        mustUpdateState && this.setState(newState);
    };
    Calendar.prototype.setDisplayDate = function (date, newSelectedDate) {
        var newDisplayDate = date_time_utils_1.getFirstDayOfMonth(date);
        var direction = newDisplayDate > this.state.displayDate ? "left" : "right";
        if (newDisplayDate !== this.state.displayDate) {
            this.setState({
                displayDate: newDisplayDate,
                transitionDirection: direction,
                selectedDate: newSelectedDate || this.state.selectedDate
            });
        }
    };
    Calendar.prototype.setSelectedDate = function (date) {
        var adjustDate = date;
        var _a = this.assignedProps, minDate = _a.minDate, maxDate = _a.maxDate;
        if (date_time_utils_1.isBeforeDate(date, minDate)) {
            adjustDate = minDate;
        }
        else if (date_time_utils_1.isAfterDate(date, maxDate)) {
            adjustDate = maxDate;
        }
        var newDisplayDate = date_time_utils_1.getFirstDayOfMonth(adjustDate);
        if (newDisplayDate !== this.state.displayDate) {
            this.setDisplayDate(newDisplayDate, adjustDate);
        }
        else {
            this.setState({
                selectedDate: adjustDate
            });
        }
    };
    Calendar.prototype.getSelectedDate = function () {
        return this.state.selectedDate;
    };
    Calendar.prototype.handleMonthChange = function (month) {
        var displayDate;
        var direction = "left";
        if (month >= 0) {
            displayDate = index_1.moment(this.state.displayDate).add(1, "month");
            direction = "right";
        }
        else {
            displayDate = index_1.moment(this.state.displayDate).subtract(1, "month");
        }
        this.setState({
            transitionDirection: direction,
            displayDate: displayDate.format()
        });
    };
    Calendar.prototype.getToolbarInteractions = function () {
        var assignedProps = object_1.assign(Calendar.defaultProps, this.props);
        return {
            prevMonth: index_1.moment(this.state.displayDate).subtract(1, "month").isAfter(assignedProps.minDate, "month"),
            nextMonth: index_1.moment(this.state.displayDate).add(1, "month").isBefore(assignedProps.maxDate, "month")
        };
    };
    Calendar.prototype.changeDisplayDate = function (event, value) {
        var assignedProps = object_1.assign(Calendar.defaultProps, this.props);
        var date = index_1.moment(this.state.displayDate);
        var newDate = date[event](1, value);
        if (newDate.isBetween(assignedProps.minDate, assignedProps.maxDate)) {
            this.setState({
                displayDate: newDate.format()
            });
        }
    };
    Calendar.prototype.handleKeyDown = function (event) {
        var code = event.keyCode;
        switch (code) {
            // left arrow
            case 37:
                this.changeDisplayDate("subtract", "month");
                event.preventDefault();
                break;
            // up arrow
            case 38:
                this.changeDisplayDate("subtract", "year");
                event.preventDefault();
                break;
            // right arrow
            case 39:
                this.changeDisplayDate("add", "month");
                event.preventDefault();
                break;
            // down arrow
            case 40:
                this.changeDisplayDate("add", "year");
                event.preventDefault();
                break;
            default:
                break;
        }
    };
    Calendar.prototype.handleSelectDate = function (date) {
        date.moment && this.setSelectedDate(date.moment.format());
        this.props.onDayClick && this.props.onDayClick();
    };
    Calendar.prototype.handleYearSelect = function (year) {
        var selectedDate = index_1.moment(this.state.selectedDate).year(year.label);
        this.setState({ displayMonthDay: true });
        this.setSelectedDate(selectedDate.format());
    };
    Calendar.prototype.handleDateDisplayYearClick = function () {
        this.setState({
            displayMonthDay: false
        });
    };
    Calendar.prototype.handleDateDisplayMonthClick = function () {
        this.setState({
            displayMonthDay: true
        });
    };
    Calendar.prototype.render = function () {
        var _a = this.assignedProps, disableYearSelection = _a.disableYearSelection, hideCalendarDate = _a.hideCalendarDate, maxDate = _a.maxDate, minDate = _a.minDate, mode = _a.mode, showNeighboringMonths = _a.showNeighboringMonths, okLabel = _a.okLabel, cancelLabel = _a.cancelLabel, onAccept = _a.onAccept, onDismiss = _a.onDismiss, autoSelect = _a.autoSelect, locale = _a.locale;
        var _b = this.state, displayDate = _b.displayDate, selectedDate = _b.selectedDate, displayMonthDay = _b.displayMonthDay, transitionDirection = _b.transitionDirection;
        var toolbarInteractions = this.getToolbarInteractions();
        return (React.createElement("div", { className: "calendar" },
            React.createElement("div", { onKeyDown: this.handleKeyDown.bind(this), className: "event-listener", tabIndex: 1 }),
            React.createElement("div", { className: classnames("calendar-root", "calendar-mode-" + mode) },
                !hideCalendarDate &&
                    React.createElement(date_display_1.DateDisplay, { locale: locale, disableYearSelection: disableYearSelection, onYearClick: this.handleDateDisplayYearClick, onMonthDayClick: this.handleDateDisplayMonthClick, selectedDate: selectedDate, monthDaySelected: displayMonthDay }),
                React.createElement("div", { className: "calendar-container" },
                    React.createElement("div", { className: "calendar-content" },
                        displayMonthDay &&
                            React.createElement("div", null,
                                React.createElement(calendar_toolbar_1.CalendarToolbar, { displayDate: displayDate, onMonthChange: this.handleMonthChange, prevMonth: toolbarInteractions.prevMonth, nextMonth: toolbarInteractions.nextMonth, locale: locale, direction: this.state.transitionDirection }),
                                React.createElement("div", { className: "calendar-weeks--root" },
                                    React.createElement(calendar_days_1.CalendarDaysOfWeek, { locale: locale, format: "dd" }),
                                    React.createElement(calendar_month_1.CalendarMonth, { autoSelect: autoSelect, displayDate: index_1.moment(displayDate).startOf("month").format(), selectedDate: selectedDate, minDate: minDate, maxDate: maxDate, showNeighboringMonths: showNeighboringMonths, onDateSelect: this.handleSelectDate, locale: locale, direction: transitionDirection }))),
                        !displayMonthDay && !disableYearSelection &&
                            React.createElement(calendar_year_1.CalendarYear, { key: "years", onYearClick: this.handleYearSelect, selectedDate: selectedDate, minDate: minDate, maxDate: maxDate })),
                    this.state.displayMonthDay && !autoSelect &&
                        React.createElement(calendar_action_buttons_1.CalendarActionButtons, { submitLabel: okLabel, cancelLabel: cancelLabel, onSubmitClick: onAccept, onCancelClick: onDismiss })))));
    };
    Calendar.defaultProps = {
        autoSelect: false,
        cancelLabel: index_1.DEFAULT_LABEL_CANCEL,
        disableYearSelection: false,
        hideCalendarDate: false,
        initialDate: index_1.moment().format(),
        locale: date_time_utils_1.getUserLanguage(),
        showNeighboringMonths: false,
        onDayClick: function () { return void 0; },
        onAccept: function () { return void 0; },
        onDismiss: function () { return void 0; },
        maxDate: index_1.moment().add(index_1.DIFFERENCE_IN_YEARS, "year").format(),
        minDate: index_1.moment().subtract(index_1.DIFFERENCE_IN_YEARS, "year").format(),
        okLabel: index_1.DEFAULT_LABEL_OK,
        mode: "landscape"
    };
    return Calendar;
}(React.Component));
exports.Calendar = Calendar;
