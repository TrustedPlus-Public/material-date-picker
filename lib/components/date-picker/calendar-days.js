"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var date_time_utils_1 = require("../core/date-time-utils");
function getNextDay(current, additionalDay) {
    if (additionalDay === void 0) { additionalDay = 1; }
    return current.add(additionalDay, "day");
}
function getDaysOfWeek(format) {
    var days = [];
    for (var i = 0; i < 7; i++) {
        var firstWeekday = date_time_utils_1.getFirstWeekday();
        var next = getNextDay(firstWeekday.date, i);
        var weekday = firstWeekday.sundayWeekday + 7;
        days.push({
            label: next.format(format),
            isWeekend: ((i === (weekday % 7) || i === (weekday - 1) % 7))
        });
    }
    return days;
}
var CalendarDaysOfWeek = /** @class */ (function (_super) {
    __extends(CalendarDaysOfWeek, _super);
    function CalendarDaysOfWeek() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CalendarDaysOfWeek.prototype.shouldComponentUpdate = function (nextProps) {
        var _a = this.props, format = _a.format, locale = _a.locale;
        return !!((format && (format !== nextProps.format)) || locale !== nextProps.locale);
    };
    CalendarDaysOfWeek.prototype.render = function () {
        var days = getDaysOfWeek(this.props.format);
        return (React.createElement("div", { className: "calendar-week--days" }, days.map(function (day) {
            var label = day.label, isWeekend = day.isWeekend;
            return React.createElement("div", { key: label, className: classnames("calendar-week--day day-name", { "day-weekend": isWeekend }) }, label);
        })));
    };
    return CalendarDaysOfWeek;
}(React.Component));
exports.CalendarDaysOfWeek = CalendarDaysOfWeek;
