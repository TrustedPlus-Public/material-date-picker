"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var index_1 = require("./index");
var date_time_utils_1 = require("../core/date-time-utils");
var slideIn_transition_group_1 = require("../slideIn-transition-group");
var day_button_1 = require("./day-button");
var DAYS_IN_WEEK = 7;
var CalendarMonth = /** @class */ (function (_super) {
    __extends(CalendarMonth, _super);
    function CalendarMonth() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CalendarMonth.prototype.shouldComponentUpdate = function (nextProps) {
        var _a = this.props, locale = _a.locale, displayDate = _a.displayDate, showNeighboringMonths = _a.showNeighboringMonths;
        var shouldUpdate = false;
        if (locale !== nextProps.locale) {
            shouldUpdate = true;
        }
        if (displayDate !== nextProps.displayDate) {
            shouldUpdate = true;
        }
        if (showNeighboringMonths !== nextProps.showNeighboringMonths) {
            shouldUpdate = true;
        }
        return shouldUpdate;
    };
    CalendarMonth.prototype.getDayElement = function (day, key, showNeighboardMonth) {
        var _this = this;
        var element = React.createElement("div", { key: key, className: "calendar-week--day" });
        if ((showNeighboardMonth && day.isNeighborMonth) || !day.isNeighborMonth) {
            element = React.createElement(day_button_1.DayButton, { key: key, ref: key, className: classnames({
                    "day-current": day.isCurrentDate,
                    "day-weekend": day.isWeekend,
                    "month-neighbor": day.isNeighborMonth
                }), isSelected: day.isSelectedDate, onClick: function () {
                    _this.props.onDateSelect(day);
                    var prevSelected = _this.lastSelectedDate;
                    _this.lastSelectedDate = key;
                    setTimeout(function () {
                        _this.refs[prevSelected] && _this.refs[prevSelected].unselect();
                        _this.refs[_this.lastSelectedDate].select();
                    }, 50);
                } }, day.label);
        }
        return element;
    };
    CalendarMonth.prototype.getWeekDays = function () {
        var props = this.props;
        var showNeighboringMonths = props.showNeighboringMonths;
        var propsDisplayDate = props.displayDate;
        var selectedDate = props.selectedDate;
        var current = index_1.moment();
        var displayDate = index_1.moment(propsDisplayDate);
        var firstDate = date_time_utils_1.getFirstDayOfMonth(propsDisplayDate);
        var lastDate = date_time_utils_1.getLastDayOfMonth(propsDisplayDate);
        var firstIsoWeekday = index_1.moment(firstDate).weekday();
        var lastWeekday = index_1.moment(lastDate).weekday();
        var daysInMonth = displayDate.daysInMonth();
        var weeksCount = Math.ceil((daysInMonth + firstIsoWeekday) / DAYS_IN_WEEK);
        var monthDays = [];
        var prevDate;
        for (var weekCounter = 0; weekCounter < weeksCount; weekCounter++) {
            var week = monthDays[weekCounter] = [];
            if (weekCounter === 0) {
                prevDate = index_1.moment(firstDate).subtract(firstIsoWeekday + 1, "day");
            }
            for (var weekDay = 0; weekDay < DAYS_IN_WEEK; weekDay++) {
                prevDate.add(1, "day");
                var day = {
                    label: "",
                    isWeekend: false,
                    isCurrentDate: false,
                    isSelectedDate: false,
                    isNeighborMonth: false,
                    moment: prevDate.clone()
                };
                if (((weekCounter === 0) && (weekDay < firstIsoWeekday)) ||
                    (((weekCounter === weeksCount - 1) && (day.moment.weekday() > lastWeekday)))) {
                    day.isNeighborMonth = true;
                }
                var dayIsoWeekday = day.moment.isoWeekday();
                day.isCurrentDate = date_time_utils_1.isSameDate(current, day.moment);
                day.isSelectedDate = date_time_utils_1.isSameDate(selectedDate, day.moment);
                day.isWeekend = (dayIsoWeekday === DAYS_IN_WEEK) || (dayIsoWeekday === DAYS_IN_WEEK - 1);
                day.label = "" + day.moment.date();
                var key = "day-" + weekDay + "-of-" + weekCounter;
                day.element = this.getDayElement(day, key, showNeighboringMonths);
                day.isSelectedDate && (this.lastSelectedDate = key);
                week.push(day);
            }
        }
        return monthDays;
    };
    CalendarMonth.prototype.render = function () {
        var weeksDays = this.getWeekDays();
        return (React.createElement(slideIn_transition_group_1.SlideInTransitionGroup, { direction: this.props.direction, className: "calendar-weeks" }, weeksDays.map(function (days, weekIndex) {
            return (React.createElement("div", { className: "calendar-week", key: "week-" + weekIndex }, days.map(function (day) {
                return day.element;
            })));
        })));
    };
    return CalendarMonth;
}(React.Component));
exports.CalendarMonth = CalendarMonth;
