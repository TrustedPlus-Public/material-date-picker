import * as React from "react";
export interface IWeekDayLabel {
    label: string;
    isWeekend: boolean;
}
export interface ICalendarDaysProps {
    format: string;
    locale: string;
}
export declare class CalendarDaysOfWeek extends React.Component<ICalendarDaysProps, {}> {
    shouldComponentUpdate(nextProps: ICalendarDaysProps): boolean;
    render(): JSX.Element;
}
