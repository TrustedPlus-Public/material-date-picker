import * as React from "react";
export interface IDayButtonProps {
    disabled?: boolean;
    className?: string;
    onClick: React.MouseEventHandler<any>;
    isSelected: boolean;
}
export declare class DayButton extends React.Component<IDayButtonProps, any> {
    constructor(props: IDayButtonProps);
    select(): void;
    unselect(): void;
    render(): JSX.Element;
}
