"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var index_1 = require("../button/index");
var slideIn_transition_group_1 = require("../slideIn-transition-group");
var index_2 = require("./index");
var CalendarToolbar = /** @class */ (function (_super) {
    __extends(CalendarToolbar, _super);
    function CalendarToolbar(props) {
        var _this = _super.call(this, props) || this;
        _this.handleClickPrevMonth = _this.handleClickPrevMonth.bind(_this);
        _this.handleClickNextMonth = _this.handleClickNextMonth.bind(_this);
        return _this;
    }
    CalendarToolbar.prototype.shouldComponentUpdate = function (nextProps) {
        var shouldUpdate = false;
        var _a = this.props, locale = _a.locale, displayDate = _a.displayDate;
        if (locale !== nextProps.locale || displayDate !== nextProps.displayDate) {
            shouldUpdate = true;
        }
        return shouldUpdate;
    };
    CalendarToolbar.prototype.handleClickPrevMonth = function () {
        this.props.onMonthChange(-1);
    };
    CalendarToolbar.prototype.handleClickNextMonth = function () {
        this.props.onMonthChange(1);
    };
    CalendarToolbar.prototype.render = function () {
        var _a = this.props, displayDate = _a.displayDate, direction = _a.direction;
        var dateFormatted = index_2.moment(displayDate).format("MMMM YYYY");
        return (React.createElement("div", { className: "calendar-toolbar--root" },
            React.createElement(index_1.IconButton, { icon: "keyboard_arrow_left", className: "calendar-toolbar--icon icon-left", onClick: this.handleClickPrevMonth }),
            React.createElement("div", { className: "calendar-toolbar--label-container" },
                React.createElement(slideIn_transition_group_1.SlideInTransitionGroup, { direction: direction },
                    React.createElement("div", { className: "calendar-toolbar--label" }, dateFormatted))),
            React.createElement(index_1.IconButton, { icon: "keyboard_arrow_right", className: "calendar-toolbar--icon icon-right", onClick: this.handleClickNextMonth })));
    };
    return CalendarToolbar;
}(React.Component));
exports.CalendarToolbar = CalendarToolbar;
