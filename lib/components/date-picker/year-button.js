"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var index_1 = require("../button/index");
var YearButton = /** @class */ (function (_super) {
    __extends(YearButton, _super);
    function YearButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    YearButton.prototype.render = function () {
        return (React.createElement(index_1.FlatButton, { onClick: this.props.onClick, className: classnames(this.props.className ? this.props.className : "", "year-button"), color: "none" }, this.props.children));
    };
    return YearButton;
}(React.Component));
exports.YearButton = YearButton;
