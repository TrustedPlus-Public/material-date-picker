import * as React from "react";
import { Moment } from "./index";
import { SlideDirection } from "../slideIn-transition-group";
import { IWeekDayLabel } from "./calendar-days";
export interface IWeekDay extends IWeekDayLabel {
    isCurrentDate: boolean;
    isNeighborMonth: boolean;
    moment: Moment | undefined;
    isSelectedDate: boolean;
    element?: any;
}
export interface ICalendarMonthProps {
    autoSelect: boolean;
    direction: SlideDirection;
    displayDate: string;
    locale: string;
    maxDate: string;
    minDate: string;
    selectedDate: string;
    showNeighboringMonths: boolean;
    onDateSelect: (day: IWeekDay) => void;
}
export declare class CalendarMonth extends React.Component<ICalendarMonthProps, {}> {
    private lastSelectedDate;
    shouldComponentUpdate(nextProps: ICalendarMonthProps): boolean;
    getDayElement(day: IWeekDay, key: string, showNeighboardMonth: boolean): any;
    getWeekDays(): Array<IWeekDay[]>;
    render(): JSX.Element;
}
