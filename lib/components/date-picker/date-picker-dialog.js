"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var calendar_1 = require("./calendar");
var DatePickerDialog = /** @class */ (function (_super) {
    __extends(DatePickerDialog, _super);
    function DatePickerDialog(props) {
        var _this = _super.call(this, props) || this;
        _this.opened = false;
        _this.state = {
            isOpened: false
        };
        _this.open = _this.open.bind(_this);
        _this.close = _this.close.bind(_this);
        _this.handleDateSelect = _this.handleDateSelect.bind(_this);
        _this.handleAccept = _this.handleAccept.bind(_this);
        _this.handleDismiss = _this.handleDismiss.bind(_this);
        _this.handleWindowKeyUp = _this.handleWindowKeyUp.bind(_this);
        _this.handleWrapperClick = _this.handleWrapperClick.bind(_this);
        return _this;
    }
    DatePickerDialog.prototype.handleDateSelect = function () {
        if (this.props.autoSelect) {
            setTimeout(this.handleAccept, 300);
        }
    };
    DatePickerDialog.prototype.componentDidUpdate = function (nextProps, nextState) {
        if (this.opened !== nextState.isOpened) {
            nextState.isOpened = this.opened;
            this.setState(nextState);
        }
    };
    DatePickerDialog.prototype.handleAccept = function () {
        var date = this.refs["calendar"].getSelectedDate();
        this.props.onAccept && this.props.onAccept(date);
        this.close();
    };
    DatePickerDialog.prototype.handleDismiss = function () {
        this.props.onDismiss && this.props.onDismiss();
        this.close();
    };
    DatePickerDialog.prototype.handleWrapperClick = function (event) {
        var target = event.target;
        if (target.classList.contains("do-close")) {
            this.close();
        }
    };
    DatePickerDialog.prototype.handleWindowKeyUp = function (event) {
        var keyCode = event.keyCode;
        var calendar = this.refs["calendar"];
        switch (keyCode) {
            case 13:
                this.handleAccept();
                break;
            case 27:
                this.handleDismiss();
                break;
            default:
                calendar.handleKeyDown(event);
                break;
        }
        event.preventDefault();
        return false;
    };
    DatePickerDialog.prototype.open = function () {
        this.opened = true;
        this.setState({
            isOpened: true
        });
    };
    DatePickerDialog.prototype.close = function () {
        this.opened = false;
        this.setState({
            isOpened: false
        });
    };
    DatePickerDialog.prototype.render = function () {
        var _a = this.props, autoSelect = _a.autoSelect, cancelLabel = _a.cancelLabel, hideCalendarDate = _a.hideCalendarDate, disableYearSelection = _a.disableYearSelection, initialDate = _a.initialDate, locale = _a.locale, showNeighboringMonths = _a.showNeighboringMonths, maxDate = _a.maxDate, minDate = _a.minDate, okLabel = _a.okLabel, mode = _a.mode;
        var rootClassName = classnames("date-picker-dialog-root", { opened: this.state.isOpened });
        return (React.createElement("div", { className: rootClassName },
            React.createElement("div", { className: "date-picker-dialog-wrapper do-close", tabIndex: 1, ref: "root", onClick: this.handleWrapperClick, onKeyUp: this.handleWindowKeyUp },
                React.createElement("div", { className: "date-picker-dialog-content", onClick: function (event) { return event.stopPropagation(); } },
                    React.createElement(calendar_1.Calendar, { ref: "calendar", hideCalendarDate: hideCalendarDate, autoSelect: autoSelect, cancelLabel: cancelLabel, disableYearSelection: disableYearSelection, initialDate: initialDate, locale: locale, showNeighboringMonths: showNeighboringMonths, onDayClick: this.handleDateSelect, onAccept: this.handleAccept, onDismiss: this.handleDismiss, maxDate: maxDate, minDate: minDate, okLabel: okLabel, mode: mode })))));
    };
    return DatePickerDialog;
}(React.Component));
exports.DatePickerDialog = DatePickerDialog;
