"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var index_1 = require("./index");
var index_2 = require("../input/index");
var date_picker_dialog_1 = require("./date-picker-dialog");
var object_1 = require("../core/object");
var date_time_utils_1 = require("../core/date-time-utils");
var DatePicker = /** @class */ (function (_super) {
    __extends(DatePicker, _super);
    function DatePicker(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            date: index_1.moment(props.defaultDate).format() || index_1.moment().format()
        };
        _this.getDate = _this.getDate.bind(_this);
        _this.handleAccept = _this.handleAccept.bind(_this);
        _this.handleDismiss = _this.handleDismiss.bind(_this);
        _this.handleInputClick = _this.handleInputClick.bind(_this);
        return _this;
    }
    DatePicker.prototype.componentWillReceiveProps = function (nextProps) {
        if (this.props.locale !== nextProps.locale || index_1.moment.locale() !== nextProps.locale) {
            index_1.moment.locale(nextProps.locale);
        }
        var newState = {};
        if (nextProps.defaultDate !== undefined && this.props.defaultDate !== nextProps.defaultDate) {
            newState.date = nextProps.defaultDate;
        }
        this.setState(newState);
    };
    DatePicker.prototype.componentWillMount = function () {
        if (index_1.moment.locale() !== this.props.locale) {
            index_1.moment.locale(this.props.locale);
        }
    };
    DatePicker.prototype.getDate = function () {
        return this.state.date;
    };
    DatePicker.prototype.handleAccept = function (date) {
        this.setState({
            date: date
        });
        this.props.onChange && this.props.onChange(date);
    };
    DatePicker.prototype.handleDismiss = function () {
        this.props.onDismiss && this.props.onDismiss();
    };
    DatePicker.prototype.handleInputClick = function () {
        this.refs["pickerDialog"].open();
        this.props.onShow && this.props.onShow();
    };
    DatePicker.prototype.render = function () {
        var _a = object_1.assign(DatePicker.defaultProps, this.props), autoSelect = _a.autoSelect, className = _a.className, disableYearSelection = _a.disableYearSelection, disabled = _a.disabled, formatDate = _a.formatDate, hideCalendarDate = _a.hideCalendarDate, locale = _a.locale, maxDate = _a.maxDate, minDate = _a.minDate, mode = _a.mode, onShow = _a.onShow, cancelLabel = _a.cancelLabel, okLabel = _a.okLabel, placeholder = _a.placeholder, showNeighboringMonths = _a.showNeighboringMonths, ico = _a.ico;
        var date = this.state.date;
        return (React.createElement("div", { className: "date-picker-root " + className },
            React.createElement(index_2.InputField, { placeholder: placeholder, disabled: disabled, value: index_1.moment(date).format("LL"), ico: ico, onClick: this.handleInputClick, onChange: function () { return void 0; } }),
            React.createElement(date_picker_dialog_1.DatePickerDialog, { autoSelect: autoSelect, cancelLabel: cancelLabel, disableYearSelection: disableYearSelection, hideCalendarDate: hideCalendarDate, locale: locale, maxDate: maxDate, minDate: minDate, mode: mode, formatDate: formatDate, okLabel: okLabel, onAccept: this.handleAccept, onDismiss: this.handleDismiss, onShow: onShow, onChangeLocale: function () { return void 0; }, onChangeShowNeighboringMonths: function () { return void 0; }, initialDate: date, showNeighboringMonths: showNeighboringMonths, ref: "pickerDialog" })));
    };
    DatePicker.defaultProps = {
        autoSelect: false,
        cancelLabel: index_1.DEFAULT_LABEL_CANCEL,
        className: "",
        defaultDate: index_1.moment().format(),
        disableYearSelection: false,
        disabled: false,
        hideCalendarDate: false,
        locale: index_1.DEFAULT_LOCALE || date_time_utils_1.getUserLanguage(),
        mode: "landscape",
        okLabel: index_1.DEFAULT_LABEL_OK,
        showNeighboringMonths: true,
        maxDate: index_1.moment().add(index_1.DIFFERENCE_IN_YEARS, "year").format(),
        minDate: index_1.moment().subtract(index_1.DIFFERENCE_IN_YEARS, "year").format()
    };
    return DatePicker;
}(React.Component));
exports.DatePicker = DatePicker;
