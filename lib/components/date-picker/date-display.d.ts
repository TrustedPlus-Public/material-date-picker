import * as React from "react";
import { SlideDirection } from "../slideIn-transition-group";
interface IDateDisplayProps {
    locale: string;
    disableYearSelection: boolean;
    monthDaySelected: boolean;
    selectedDate: string;
    onYearClick: () => void;
    onMonthDayClick: () => void;
}
interface IDateDisplayState {
    transitionDirection: SlideDirection;
    monthDaySelected: boolean;
}
export declare class DateDisplay extends React.Component<IDateDisplayProps, IDateDisplayState> {
    constructor(props: IDateDisplayProps);
    componentWillMount(): void;
    componentWillReceiveProps(nextProps: IDateDisplayProps): void;
    shouldComponentUpdate(nextProps: IDateDisplayProps): boolean;
    handleYearClick(): void;
    handleMonthDayClick(): void;
    render(): JSX.Element;
}
export {};
