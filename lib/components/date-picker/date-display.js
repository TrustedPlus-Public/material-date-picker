"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var index_1 = require("./index");
var slideIn_transition_group_1 = require("../slideIn-transition-group");
var DateDisplay = /** @class */ (function (_super) {
    __extends(DateDisplay, _super);
    function DateDisplay(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            transitionDirection: "up",
            monthDaySelected: true
        };
        _this.handleYearClick = _this.handleYearClick.bind(_this);
        _this.handleMonthDayClick = _this.handleMonthDayClick.bind(_this);
        return _this;
    }
    DateDisplay.prototype.componentWillMount = function () {
        var monthDaySelected = this.props.monthDaySelected;
        if (this.state.monthDaySelected !== monthDaySelected) {
            this.setState({ monthDaySelected: monthDaySelected });
        }
    };
    DateDisplay.prototype.componentWillReceiveProps = function (nextProps) {
        var newState = {};
        var mustUpdateState = false;
        var selectedDate = this.props.selectedDate;
        if (nextProps.selectedDate !== selectedDate) {
            newState.transitionDirection = index_1.moment(nextProps.selectedDate).isAfter(index_1.moment(selectedDate)) ? "up" : "down";
            mustUpdateState = true;
        }
        if (nextProps.monthDaySelected !== undefined && nextProps.monthDaySelected !== this.state.monthDaySelected) {
            newState.monthDaySelected = nextProps.monthDaySelected;
            mustUpdateState = true;
        }
        mustUpdateState && this.setState(newState);
    };
    DateDisplay.prototype.shouldComponentUpdate = function (nextProps) {
        var shouldUpdate = false;
        var _a = this.props, monthDaySelected = _a.monthDaySelected, selectedDate = _a.selectedDate, locale = _a.locale;
        if (nextProps.selectedDate !== selectedDate) {
            shouldUpdate = true;
        }
        if (nextProps.monthDaySelected !== undefined && nextProps.monthDaySelected !== monthDaySelected) {
            shouldUpdate = true;
        }
        if (nextProps.locale !== locale) {
            shouldUpdate = true;
        }
        return shouldUpdate;
    };
    DateDisplay.prototype.handleYearClick = function () {
        if (this.props.onYearClick && !this.props.disableYearSelection && this.state.monthDaySelected) {
            this.props.onYearClick();
        }
    };
    DateDisplay.prototype.handleMonthDayClick = function () {
        if (this.props.onMonthDayClick && !this.state.monthDaySelected) {
            this.props.onMonthDayClick();
        }
    };
    DateDisplay.prototype.render = function () {
        var selectedDate = this.props.selectedDate;
        var monthDaySelected = this.state.monthDaySelected;
        var date = index_1.moment(selectedDate);
        var year = index_1.moment(date).format("YYYY");
        var dateTimeFormatted = date.format("llll");
        var dateTimeLabel = dateTimeFormatted.slice(0, dateTimeFormatted.indexOf(date.year().toString()) - 1);
        return (React.createElement("div", { className: "calendar-date-display--root" },
            React.createElement("div", { className: classnames("calendar-display--year-title", "calendar-display-date--title", { "calendar-date-display--title-active": !monthDaySelected }) },
                React.createElement(DisplayYear, { direction: this.state.transitionDirection, onClick: this.handleYearClick, year: year })),
            React.createElement("div", { className: classnames("calendar-display--date-month-title calendar-display-date--title", { "calendar-date-display--title-active": monthDaySelected }), onClick: this.handleMonthDayClick }, dateTimeLabel)));
    };
    return DateDisplay;
}(React.Component));
exports.DateDisplay = DateDisplay;
var DisplayYear = /** @class */ (function (_super) {
    __extends(DisplayYear, _super);
    function DisplayYear() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DisplayYear.prototype.shouldComponentUpdate = function (nextProps) {
        return this.props.year !== nextProps.year;
    };
    DisplayYear.prototype.render = function () {
        var _a = this.props, year = _a.year, direction = _a.direction, onClick = _a.onClick;
        return (React.createElement(slideIn_transition_group_1.SlideInTransitionGroup, { direction: direction },
            React.createElement("div", { key: "year-" + year, className: "date-display--label-year", onClick: onClick }, year)));
    };
    return DisplayYear;
}(React.Component));
