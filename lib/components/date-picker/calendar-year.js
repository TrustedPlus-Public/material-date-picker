"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_dom_1 = require("react-dom");
var classnames = require("classnames");
var index_1 = require("./index");
var year_button_1 = require("./year-button");
var CalendarYear = /** @class */ (function (_super) {
    __extends(CalendarYear, _super);
    function CalendarYear() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CalendarYear.prototype.componentDidMount = function () {
        this.scrollToSelectedYear();
    };
    CalendarYear.prototype.componentDidUpdate = function () {
        this.scrollToSelectedYear();
    };
    CalendarYear.prototype.getYearElement = function (year, handleClick) {
        var key = "yb" + year.label;
        return (React.createElement(year_button_1.YearButton, { key: key, ref: key, onClick: function () { return handleClick(year); }, className: classnames("calendar-years--year", {
                "year-selected": year.isSelected,
                "year-current": year.isCurrent
            }) }, year.label));
    };
    CalendarYear.prototype.getYears = function () {
        var _a = this.props, minDate = _a.minDate, maxDate = _a.maxDate, selectedDate = _a.selectedDate;
        var minYear = index_1.moment(minDate).year();
        var maxYear = index_1.moment(maxDate).year();
        var years = [];
        var currentYear = index_1.moment().year();
        var selectedYear = index_1.moment(selectedDate).year();
        for (var i = minYear; i < maxYear; i++) {
            var year = {
                isSelected: i === selectedYear,
                isCurrent: i === currentYear,
                label: i
            };
            year.element = this.getYearElement(year, this.props.onYearClick);
            years.push(year);
        }
        return years;
    };
    CalendarYear.prototype.scrollToSelectedYear = function () {
        var container = react_dom_1.findDOMNode(this);
        var yearButtonNode = react_dom_1.findDOMNode((document.querySelectorAll(".year-selected")[0]).parentNode);
        var containerHeight = container.clientHeight;
        var yearButtonNodeHeight = yearButtonNode.clientHeight || 32;
        var scrollYOffset = (yearButtonNode.offsetTop + yearButtonNodeHeight / 2) - containerHeight / 2;
        container.scrollTop = scrollYOffset;
    };
    CalendarYear.prototype.render = function () {
        var years = this.getYears();
        return (React.createElement("div", { className: "calendar-year--root" }, years.map(function (year) {
            return year.element;
        })));
    };
    return CalendarYear;
}(React.Component));
exports.CalendarYear = CalendarYear;
