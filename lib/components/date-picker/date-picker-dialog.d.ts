import * as React from "react";
import { Mode } from "./index";
interface IDatePickerDialogProps {
    autoSelect: boolean;
    cancelLabel: string;
    disableYearSelection: boolean;
    formatDate: string;
    hideCalendarDate: boolean;
    showNeighboringMonths: boolean;
    locale: string;
    maxDate: string;
    minDate: string;
    mode: Mode;
    okLabel: string;
    onChangeLocale: (locale: string) => void;
    onChangeShowNeighboringMonths: (show: boolean) => void;
    onAccept: (date: string) => void;
    onDismiss: () => void;
    onShow: () => void;
    initialDate: string;
}
interface IDatePickerDialogState {
    isOpened: boolean;
}
export declare class DatePickerDialog extends React.Component<IDatePickerDialogProps, IDatePickerDialogState> {
    private opened;
    constructor(props: IDatePickerDialogProps);
    private handleDateSelect;
    componentDidUpdate(nextProps: any, nextState: any): void;
    private handleAccept;
    private handleDismiss;
    private handleWrapperClick;
    private handleWindowKeyUp;
    open(): void;
    close(): void;
    render(): JSX.Element;
}
export {};
