import * as React from "react";
interface IYearButtonProps {
    onClick: () => void;
    className?: string;
    key?: any;
}
export declare class YearButton extends React.Component<IYearButtonProps> {
    render(): JSX.Element;
}
export {};
