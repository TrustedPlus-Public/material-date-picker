import * as React from "react";
interface IDatePickerState {
    date: string;
}
export interface IDatePickerProps {
    autoSelect?: boolean;
    cancelLabel?: string;
    className?: string;
    defaultDate?: string | Date;
    disableYearSelection?: boolean;
    disabled?: boolean;
    hideCalendarDate?: boolean;
    locale?: string;
    maxDate?: string;
    minDate?: string;
    mode?: "portrait" | "landscape";
    okLabel?: string;
    onChange?: (date: string) => void;
    onDismiss?: () => void;
    onShow?: () => void;
    placeholder?: string;
    showNeighboringMonths?: boolean;
    ico?: string;
}
export declare class DatePicker extends React.Component<IDatePickerProps, IDatePickerState> {
    static defaultProps: IDatePickerProps;
    constructor(props: IDatePickerProps);
    componentWillReceiveProps(nextProps: IDatePickerProps): void;
    componentWillMount(): void;
    getDate(): string;
    private handleAccept;
    private handleDismiss;
    private handleInputClick;
    render(): JSX.Element;
}
export {};
