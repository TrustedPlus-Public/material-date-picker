import * as moment from "moment";
import { Moment } from "moment";
export { Moment, moment };
export declare type Mode = "landscape" | "portrait" | undefined;
export declare const DIFFERENCE_IN_YEARS: number;
export declare const DEFAULT_LABEL_OK = "\u041E\u043A";
export declare const DEFAULT_LABEL_CANCEL = "\u041E\u0442\u043C\u0435\u043D\u0430";
export declare const DEFAULT_LOCALE = "ru";
export declare const supportedLocales: {
    value: string;
    label: string;
}[];
export * from "./date-picker";
