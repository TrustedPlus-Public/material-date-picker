import * as React from "react";
export interface ICalendarYearProps {
    key?: string;
    onYearClick: (year: any) => void;
    selectedDate: string;
    minDate: string;
    maxDate: string;
    mode?: any;
}
export declare class CalendarYear extends React.Component<ICalendarYearProps> {
    componentDidMount(): void;
    componentDidUpdate(): void;
    getYearElement(year: any, handleClick: (year: any) => void): JSX.Element;
    getYears(): any[];
    scrollToSelectedYear(): void;
    render(): JSX.Element;
}
