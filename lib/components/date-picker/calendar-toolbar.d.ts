import * as React from "react";
import { SlideDirection } from "../slideIn-transition-group";
export interface ICalendarToolbarProps {
    displayDate: string;
    onMonthChange: (dir: number) => void;
    nextMonth: boolean;
    prevMonth: boolean;
    locale: string;
    direction: SlideDirection;
}
interface ICalendarToolbarState {
}
export declare class CalendarToolbar extends React.Component<ICalendarToolbarProps, ICalendarToolbarState> {
    constructor(props: ICalendarToolbarProps);
    shouldComponentUpdate(nextProps: ICalendarToolbarProps): boolean;
    handleClickPrevMonth(): void;
    handleClickNextMonth(): void;
    render(): JSX.Element;
}
export {};
