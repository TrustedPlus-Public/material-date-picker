"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var index_1 = require("../button/index");
var CalendarActionButtons = /** @class */ (function (_super) {
    __extends(CalendarActionButtons, _super);
    function CalendarActionButtons() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CalendarActionButtons.prototype.render = function () {
        var _a = this.props, cancelTitle = _a.cancelTitle, cancelLabel = _a.cancelLabel, submitLabel = _a.submitLabel, submitTitle = _a.submitTitle, onCancelClick = _a.onCancelClick, onSubmitClick = _a.onSubmitClick;
        return (React.createElement("div", { className: "calendar-action-buttons" },
            React.createElement(index_1.FlatButton, { color: "date-blue", className: "calendar-action-button--cancel", title: cancelTitle, onClick: onCancelClick }, cancelLabel),
            React.createElement(index_1.FlatButton, { className: "calendar-action-button--submit", color: "date-blue", title: submitTitle, onClick: onSubmitClick }, submitLabel)));
    };
    return CalendarActionButtons;
}(React.Component));
exports.CalendarActionButtons = CalendarActionButtons;
