export declare type ComponentSizeType = "small" | "medium" | "large";
export declare type ComponentFontSizeType = "display-1" | "display-2" | "display-3" | "display-4" | "headline" | "title" | "subheading" | "body-1" | "body-2" | "caption" | "button";
export interface ComponentBase {
    className?: string;
}
export interface ComponentSize {
    size?: ComponentSizeType;
}
export interface ComponentFontSize {
    fontSize?: ComponentFontSizeType;
}
export interface ComponentColor {
    color?: string;
}
export interface ComponentColorSheme {
    colorSheme?: string;
}
export interface ComponentBgColor {
    bgColor?: string;
}
export interface ComponentTitle {
    label: string;
    description?: string;
}
export declare type TooltipPosition = "right" | "bottom" | "left";
