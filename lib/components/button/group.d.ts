import * as React from "react";
import { ComponentBase } from "../types";
export interface IButtonGroupProps extends ComponentBase {
    position?: "top" | "bottom";
    align?: "left" | "center" | "right";
}
export declare class ButtonGroup extends React.Component<IButtonGroupProps, {}> {
    static defaultProps: IButtonGroupProps;
    render(): JSX.Element;
}
