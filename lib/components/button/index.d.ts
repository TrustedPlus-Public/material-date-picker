import * as React from "react";
import { ComponentSizeType, TooltipPosition } from "../types";
export * from "./group";
declare type BaseButtonPosition = "left" | "center" | "right";
export interface IBaseButtonProps {
    color?: string;
    title?: string;
    bgColor?: string;
    onClick?: React.MouseEventHandler<any>;
    disabled?: boolean;
    className?: string;
    to?: string;
    hidden?: boolean;
    href?: string;
    target?: "_blank" | "_self" | "_parent" | "_top";
    position?: BaseButtonPosition;
    tooltip?: string;
    tooltipPosition?: TooltipPosition;
}
export interface IFlatButtonProps extends IBaseButtonProps {
}
export declare class FlatButton extends React.Component<IFlatButtonProps, {}> {
    static defaultProps: IRaisedButtonProps;
    render(): JSX.Element;
}
export interface IRaisedButtonProps extends IBaseButtonProps {
}
export declare class RaisedButton extends React.Component<IRaisedButtonProps, {}> {
    static defaultProps: IRaisedButtonProps;
    render(): JSX.Element;
}
export interface IRoundButtonProps extends IBaseButtonProps {
    size?: ComponentSizeType;
}
export declare class RoundButton extends React.Component<IRoundButtonProps, {}> {
    static defaultProps: IRoundButtonProps;
    render(): JSX.Element;
}
export interface IIconButtonBaseProps extends IRoundButtonProps {
}
export interface IIconButtonProps extends IIconButtonBaseProps {
    icon: string;
}
export declare class IconButton extends React.Component<IIconButtonProps, {}> {
    static defaultProps: IIconButtonProps;
    render(): JSX.Element;
}
