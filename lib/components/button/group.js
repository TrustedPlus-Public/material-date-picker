"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classname = require("classnames");
var ButtonGroup = /** @class */ (function (_super) {
    __extends(ButtonGroup, _super);
    function ButtonGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ButtonGroup.prototype.render = function () {
        var props = this.props;
        var names = ["btn-group", props.className, this.props.position ? "pos-" + this.props.position : null, this.props.align ? "align-" + this.props.align : null];
        var name = classname(names);
        return (React.createElement("div", { className: "" + name },
            this.props.children,
            React.createElement("div", { className: "clear" })));
    };
    ButtonGroup.defaultProps = {};
    return ButtonGroup;
}(React.Component));
exports.ButtonGroup = ButtonGroup;
