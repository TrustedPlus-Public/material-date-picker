"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_router_1 = require("react-router");
var core = require("../core");
var icon_1 = require("../icon");
__export(require("./group"));
var BaseButton = /** @class */ (function (_super) {
    __extends(BaseButton, _super);
    function BaseButton(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    BaseButton.prototype.render = function () {
        if (this.props.hidden)
            return null;
        var className = ["btn"];
        if (this.props.disabled)
            className.push("disabled");
        if (this.props.className)
            className.push(this.props.className);
        var _a = this.props, position = _a.position, tooltip = _a.tooltip, tooltipPosition = _a.tooltipPosition;
        var rootProps = {};
        position && (rootProps["className"] = "pos-" + position);
        if (this.props.tooltip) {
            rootProps["data-tooltip"] = tooltip;
            rootProps["data-tooltip-position"] = tooltipPosition ? tooltipPosition : "top";
        }
        return (React.createElement("div", __assign({ className: this.props.position ? "pos-" + this.props.position : undefined }, rootProps), !this.props.disabled && this.props.to ?
            React.createElement(react_router_1.Link, { className: className.join(" "), to: this.props.to, onClick: this.props.onClick, title: this.props.title },
                React.createElement("div", { className: "background" }),
                React.createElement("div", { className: "date-content" }, this.props.children))
            :
                React.createElement("a", { className: className.join(" "), onClick: !this.props.disabled ? this.props.onClick : undefined, href: !this.props.disabled ? this.props.href : undefined, title: this.props.title, target: this.props.target },
                    React.createElement("div", { className: "background bg-" + this.props.bgColor }),
                    React.createElement("div", { className: "date-content " + this.props.color }, this.props.children))));
    };
    BaseButton.defaultProps = {
        color: "date-black",
        bgColor: "white"
    };
    return BaseButton;
}(React.Component));
var FlatButton = /** @class */ (function (_super) {
    __extends(FlatButton, _super);
    function FlatButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlatButton.prototype.render = function () {
        var props = core.assign({}, this.props);
        props.bgColor = props.color;
        props.className = "flat " + props.className;
        return (React.createElement(BaseButton, __assign({}, props)));
    };
    FlatButton.defaultProps = {
        color: "date-black",
        className: ""
    };
    return FlatButton;
}(React.Component));
exports.FlatButton = FlatButton;
var RaisedButton = /** @class */ (function (_super) {
    __extends(RaisedButton, _super);
    function RaisedButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RaisedButton.prototype.render = function () {
        var props = core.assign({}, this.props);
        props.className = "raised " + props.className;
        return (React.createElement(BaseButton, __assign({}, props)));
    };
    RaisedButton.defaultProps = {
        bgColor: "date-blue",
        color: "white",
        className: ""
    };
    return RaisedButton;
}(React.Component));
exports.RaisedButton = RaisedButton;
var RoundButton = /** @class */ (function (_super) {
    __extends(RoundButton, _super);
    function RoundButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RoundButton.prototype.render = function () {
        var props = core.assign({}, this.props);
        props.bgColor = props.color;
        props.className = "round " + this.props.size + " " + props.className;
        return (React.createElement(BaseButton, __assign({}, props)));
    };
    RoundButton.defaultProps = {
        size: "medium",
        color: "date-black",
        className: ""
    };
    return RoundButton;
}(React.Component));
exports.RoundButton = RoundButton;
var IconButton = /** @class */ (function (_super) {
    __extends(IconButton, _super);
    function IconButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IconButton.prototype.render = function () {
        var props = core.assign({}, this.props);
        props.className = "icon " + props.className;
        return (React.createElement(RoundButton, __assign({}, props),
            React.createElement(icon_1.Icon, { name: this.props.icon })));
    };
    IconButton.defaultProps = {
        icon: "",
        size: "small",
        className: ""
    };
    return IconButton;
}(React.Component));
exports.IconButton = IconButton;
