"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var Icon = /** @class */ (function (_super) {
    __extends(Icon, _super);
    function Icon(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    Icon.prototype.render = function () {
        var _a = this.props, name = _a.name, color = _a.color, desc = _a.desc;
        var className = classnames("material-icons", color, desc);
        return (React.createElement("i", { className: className }, name));
    };
    Icon.defaultProps = {
        name: "",
    };
    return Icon;
}(React.Component));
exports.Icon = Icon;
