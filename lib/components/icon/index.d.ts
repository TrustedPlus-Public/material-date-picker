import * as React from "react";
export interface IIconProps {
    name: string;
    color?: string;
    desc?: string;
}
export declare class Icon extends React.Component<IIconProps, {}> {
    constructor(props: IIconProps);
    static defaultProps: IIconProps;
    render(): JSX.Element;
}
