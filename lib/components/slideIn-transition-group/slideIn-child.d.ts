import * as React from "react";
export interface ISlideInChildProps {
    key?: any;
    enterDelay?: any;
    maxScale?: any;
    minScale?: any;
    className?: string;
}
export declare class SlideInChild extends React.Component<ISlideInChildProps, {}> {
    render(): JSX.Element;
}
