"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var CSSTransitionGroup = require("react-addons-css-transition-group");
var slideIn_child_1 = require("./slideIn-child");
var SlideInTransitionGroup = /** @class */ (function (_super) {
    __extends(SlideInTransitionGroup, _super);
    function SlideInTransitionGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SlideInTransitionGroup.prototype.render = function () {
        var transitionOptions = {
            className: "animation-wrapper-container",
            transitionName: "fade",
            transitionEnterTimeout: 450,
            transitionLeaveTimeout: 450
        };
        var direction = this.props.direction;
        return (React.createElement("div", { className: "animation-wrapper direction-" + direction },
            React.createElement(CSSTransitionGroup, __assign({}, transitionOptions), React.cloneElement(React.createElement(slideIn_child_1.SlideInChild, { enterDelay: 0, maxScale: 0, minScale: 0, className: "animation-wrapper-content " + (this.props.className || "") }), { key: Math.random().toString(16).substr(-12), children: this.props.children }))));
    };
    return SlideInTransitionGroup;
}(React.Component));
exports.SlideInTransitionGroup = SlideInTransitionGroup;
