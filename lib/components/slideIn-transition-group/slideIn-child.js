"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var SlideInChild = /** @class */ (function (_super) {
    __extends(SlideInChild, _super);
    function SlideInChild() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SlideInChild.prototype.render = function () {
        var children = this.props.children;
        return (React.createElement("div", { key: Math.random().toString(16).substr(-12), className: this.props.className }, children));
    };
    return SlideInChild;
}(React.Component));
exports.SlideInChild = SlideInChild;
