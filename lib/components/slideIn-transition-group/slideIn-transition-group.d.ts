import * as React from "react";
export declare type SlideDirection = "right" | "left" | "up" | "down";
interface ISlideInTransitionGroupProps {
    direction: SlideDirection;
    className?: string;
}
export declare class SlideInTransitionGroup extends React.Component<ISlideInTransitionGroupProps, {}> {
    render(): JSX.Element;
}
export {};
