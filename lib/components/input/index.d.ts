import * as React from "react";
import { ComponentColorSheme } from "../types";
export interface IInputFieldProps {
    className?: string;
    hidden?: boolean;
    readOnly?: boolean;
    autoFocus?: boolean;
    placeholder?: string;
    error?: string;
    name?: string;
    onKeyUp?: React.KeyboardEventHandler<HTMLInputElement>;
    onChange?: React.FormEventHandler<HTMLInputElement>;
    onBlur?: React.FocusEventHandler<HTMLInputElement>;
    onInput?: React.FocusEventHandler<HTMLInputElement>;
    onClick?: React.MouseEventHandler<HTMLInputElement>;
    disabled?: boolean;
    value?: string;
    defaultValue?: string;
    label?: string;
    required?: boolean;
    description?: string;
    type?: string;
    tabIndex?: number;
    autoComplete?: boolean;
    fontSize?: "display-1" | "display-2" | "display-3" | "display-4" | "headline" | "title" | "subheading" | "body-1" | "body-2" | "caption" | "button" | "input";
    color?: string;
    colorSheme?: ComponentColorSheme;
    icon?: string;
    ico?: string;
}
export interface IInputFieldState {
    focused?: boolean;
}
export declare class InputField extends React.Component<IInputFieldProps, IInputFieldState> {
    static defaultProps: IInputFieldProps;
    id: string;
    constructor(props: IInputFieldProps);
    input: HTMLInputElement;
    onFocusHandle(e: React.FocusEvent<HTMLInputElement>): void;
    onBlurHandle(e: React.FocusEvent<HTMLInputElement>): void;
    render(): JSX.Element;
}
