"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var classnames = require("classnames");
var core_1 = require("../core");
var icon_1 = require("../icon");
var InputField = /** @class */ (function (_super) {
    __extends(InputField, _super);
    function InputField(props) {
        var _this = _super.call(this, props) || this;
        _this.id = core_1.RandomId();
        _this.state = {
            focused: false
        };
        _this.onFocusHandle = _this.onFocusHandle.bind(_this);
        _this.onBlurHandle = _this.onBlurHandle.bind(_this);
        return _this;
    }
    InputField.prototype.onFocusHandle = function (e) {
        this.setState({ focused: true });
        this.props.onInput && this.props.onInput(e);
    };
    InputField.prototype.onBlurHandle = function (e) {
        this.setState({ focused: false });
        this.props.onBlur && this.props.onBlur(e);
    };
    InputField.prototype.render = function () {
        var _this = this;
        var id = this.id;
        var label = null;
        if (this.props.label) {
            var isActive = (this.props.value || this.props.defaultValue || (this.input && this.input.value));
            var isFocus_1 = (this.state.focused);
            var classLabel = classnames([
                (isActive ? "active" : ""),
                (isFocus_1 ? "focus" : ""),
                (this.props.fontSize ? "font-" + this.props.fontSize : null),
                (this.props.colorSheme ? "sheme-" + this.props.colorSheme : null),
                (this.props.color ? this.props.color : null)
            ]);
            label = (React.createElement("label", { className: classLabel, onClick: function (e) { e.stopPropagation(); _this.input.focus(); } },
                this.props.label,
                this.props.required ? React.createElement("span", { className: "font-red" }, "*") : null));
        }
        var error = null;
        var description = null;
        if (this.props.error) {
            error = (React.createElement("div", { className: "input-error" },
                React.createElement("span", { className: "error-color" }, this.props.error)));
        }
        else if (this.props.description) {
            description = (React.createElement("div", { className: "input-description " + (this.props.colorSheme ? "sheme-" + this.props.colorSheme : null) },
                React.createElement("span", null, this.props.description)));
        }
        var icon = null;
        var isFocus = (this.state.focused);
        var classIcon = classnames([
            (isFocus ? "focus" : ""),
            (this.props.colorSheme ? "sheme-" + this.props.colorSheme : null),
            (this.props.description || this.props.error ? "wd" : null)
        ]);
        if (this.props.icon) {
            icon = (React.createElement(icon_1.Icon, { name: this.props.icon, desc: classIcon }));
        }
        var classInput = classnames([
            (this.props.label ? "wl" : null),
            (this.props.description ? "wd" : null),
            (this.props.error ? "we" : null),
            (this.props.error ? " invalid" : ""),
            (this.props.fontSize ? "font-" + this.props.fontSize : null),
            (this.props.colorSheme ? "sheme-" + this.props.colorSheme : null),
            (this.props.color ? this.props.color : null)
        ]);
        var classInputField = classnames([
            (this.props.disabled ? "disabled" : null),
            (this.props.icon ? "wi" : null),
            (this.props.className || "")
        ]);
        var inputStyle = this.props.ico ? {
            background: "url(" + this.props.ico + ")",
            padding: "6px 6px 6px 0",
            backgroundRepeat: "no-repeat",
            backgroundPositionX: "right",
            backgroundPositionY: "center",
            cursor: "pointer",
            opacity: 0.87,
            backgroundSize: "20px",
        } : {};
        return (React.createElement("div", { className: "field-input col " + classInputField, hidden: this.props.hidden },
            icon,
            React.createElement("div", { className: "inputBox" },
                React.createElement("input", { style: inputStyle, ref: function (e) { _this.input = e; }, id: id, hidden: this.props.hidden, readOnly: this.props.readOnly, autoFocus: this.props.autoFocus, placeholder: this.props.placeholder, type: this.props.type || "text", className: classInput, name: this.props.name, onKeyUp: this.props.onKeyUp, onFocus: this.onFocusHandle, onBlur: this.onBlurHandle, onInput: this.props.onInput, onChange: this.props.onChange, onClick: this.props.onClick, disabled: this.props.disabled, value: this.props.value, defaultValue: this.props.defaultValue, tabIndex: this.props.tabIndex, autoComplete: !this.props.autoComplete ? "off" : undefined }),
                label,
                error,
                description)));
    };
    InputField.defaultProps = {
        fontSize: "input",
        colorSheme: "light"
    };
    return InputField;
}(React.Component));
exports.InputField = InputField;
