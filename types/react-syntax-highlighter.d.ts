declare namespace react_syntax_highlighter {
    interface Style {

    }
    type Language = "html" | "javascript" | "php";
}

declare module "react-syntax-highlighter" {
    import * as React from "react";
    // <SyntaxHighlighter language="javascript" style={docco}>{codeString}</SyntaxHighlighter>
    interface SyntaxHighlighterProps {
        language: react_syntax_highlighter.Language;
        style: react_syntax_highlighter.Style
    }

    class SyntaxHighlighter extends React.Component<SyntaxHighlighterProps, any>{ }

    export default SyntaxHighlighter;
}
declare module "react-syntax-highlighter/dist/styles" {
    export const agate: react_syntax_highlighter.Style;
    export const androidstudio: react_syntax_highlighter.Style;
    export const arduinoLight: react_syntax_highlighter.Style;
    export const arta: react_syntax_highlighter.Style;
    export const ascetic: react_syntax_highlighter.Style;
    export const atelierCaveDark: react_syntax_highlighter.Style;
    export const atelierCaveLight: react_syntax_highlighter.Style;
    export const atelierDuneDark: react_syntax_highlighter.Style;
    export const atelierDuneLight: react_syntax_highlighter.Style;
    export const atelierEstuaryDark: react_syntax_highlighter.Style;
    export const atelierEstuaryLight: react_syntax_highlighter.Style;
    export const atelierForestDark: react_syntax_highlighter.Style;
    export const atelierForestLight: react_syntax_highlighter.Style;
    export const atelierHeathDark: react_syntax_highlighter.Style;
    export const atelierHeathLight: react_syntax_highlighter.Style;
    export const atelierLakesideDark: react_syntax_highlighter.Style;
    export const atelierLakesideLight: react_syntax_highlighter.Style;
    export const atelierPlateauDark: react_syntax_highlighter.Style;
    export const atelierPlateauLight: react_syntax_highlighter.Style;
    export const atelierSavannaDark: react_syntax_highlighter.Style;
    export const atelierSavannaLight: react_syntax_highlighter.Style;
    export const atelierSeasideDark: react_syntax_highlighter.Style;
    export const atelierSeasideLight: react_syntax_highlighter.Style;
    export const atelierSulphurpoolDark: react_syntax_highlighter.Style;
    export const atelierSulphurpoolLight: react_syntax_highlighter.Style;
    export const brownPaper: react_syntax_highlighter.Style;
    export const codepenEmbed: react_syntax_highlighter.Style;
    export const colorBrewer: react_syntax_highlighter.Style;
    export const dark: react_syntax_highlighter.Style;
    export const darkula: react_syntax_highlighter.Style;
    export const defaultStyle: react_syntax_highlighter.Style;
    export const docco: react_syntax_highlighter.Style;
    export const dracula: react_syntax_highlighter.Style;
    export const far: react_syntax_highlighter.Style;
    export const foundation: react_syntax_highlighter.Style;
    export const githubGist: react_syntax_highlighter.Style;
    export const github: react_syntax_highlighter.Style;
    export const googlecode: react_syntax_highlighter.Style;
    export const grayscale: react_syntax_highlighter.Style;
    export const gruvboxDark: react_syntax_highlighter.Style;
    export const gruvboxLight: react_syntax_highlighter.Style;
    export const hopscotch: react_syntax_highlighter.Style;
    export const hybrid: react_syntax_highlighter.Style;
    export const idea: react_syntax_highlighter.Style;
    export const irBlack: react_syntax_highlighter.Style;
    export const kimbieDark: react_syntax_highlighter.Style;
    export const kimbieLight: react_syntax_highlighter.Style;
    export const magula: react_syntax_highlighter.Style;
    export const monoBlue: react_syntax_highlighter.Style;
    export const monokaiSublime: react_syntax_highlighter.Style;
    export const monokai: react_syntax_highlighter.Style;
    export const obsidian: react_syntax_highlighter.Style;
    export const paraisoDark: react_syntax_highlighter.Style;
    export const paraisoLight: react_syntax_highlighter.Style;
    export const pojoaque: react_syntax_highlighter.Style;
    export const purebasic: react_syntax_highlighter.Style;
    export const qtcreatorDark: react_syntax_highlighter.Style;
    export const qtcreatorLight: react_syntax_highlighter.Style;
    export const railscasts: react_syntax_highlighter.Style;
    export const rainbow: react_syntax_highlighter.Style;
    export const schoolBook: react_syntax_highlighter.Style;
    export const solarizedDark: react_syntax_highlighter.Style;
    export const solarizedLight: react_syntax_highlighter.Style;
    export const sunburst: react_syntax_highlighter.Style;
    export const tomorrowNightBlue: react_syntax_highlighter.Style;
    export const tomorrowNightBright: react_syntax_highlighter.Style;
    export const tomorrowNightEighties: react_syntax_highlighter.Style;
    export const tomorrowNight: react_syntax_highlighter.Style;
    export const tomorrow: react_syntax_highlighter.Style;
    export const vs: react_syntax_highlighter.Style;
    export const xcode: react_syntax_highlighter.Style;
    export const xt256: react_syntax_highlighter.Style;
    export const zenburn: react_syntax_highlighter.Style;

}